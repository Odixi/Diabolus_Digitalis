﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogManager  {

    private readonly string playerName;
    private readonly Queue<Dialog> firstWaveDialogs = new Queue<Dialog>();
    private readonly Queue<Dialog> secondWaveDialogs = new Queue<Dialog>();
    private readonly Queue<Dialog> thirdWaveDialogs = new Queue<Dialog>();
    private readonly Queue<Dialog> fourthWaveDialogs = new Queue<Dialog>();
    private readonly Queue<Dialog> bossWaveDialogs = new Queue<Dialog>();
    private readonly Queue<Dialog> bossPauseDialogs = new Queue<Dialog>();

    public DialogManager(string playerName)
    {
        this.playerName = playerName;
        initFirstLevelDialogs();
        initBossPauseDialogs();
    }

    private void initFirstLevelDialogs()
    {
        //1st wave
        firstWaveDialogs.Enqueue(new Dialog("Ship AI", "There are multiple enemies in front of us.", 3));
        firstWaveDialogs.Enqueue(new Dialog(playerName, "Ok, let's rock and roll!", 3));
        firstWaveDialogs.Enqueue(new Dialog("Ship AI", "All enemies destroyed.", 3));
		firstWaveDialogs.Enqueue(new Dialog(playerName, "Good. Try to get a radio connection to any friendly ship if possible.", 5));
        firstWaveDialogs.Enqueue(new Dialog("Ship AI", "Understood.", 2));

        //2nd wave
        secondWaveDialogs.Enqueue(new Dialog("Ship AI", "Enemy ships detected, battle mode activated.", 3));
        secondWaveDialogs.Enqueue(new Dialog(playerName, "Bring it on!", 2));
        secondWaveDialogs.Enqueue(new Dialog("Ship AI", "No friendly ships nearby, failed to establish a radio connection.", 5));
        secondWaveDialogs.Enqueue(new Dialog(playerName, "Darn it! Why do those ships keep attackin us?", 3));
        secondWaveDialogs.Enqueue(new Dialog(playerName, "What is this place anyway? What is this universe?", 3));
        secondWaveDialogs.Enqueue(new Dialog("Ship AI", "Cannot identify this universe. No data available.", 3));
        secondWaveDialogs.Enqueue(new Dialog(playerName, "What? You're supposed to have every known universe in your database.", 4));
        secondWaveDialogs.Enqueue(new Dialog("Ship AI", "True, which means this is a new universe we have discovered.", 4));
        secondWaveDialogs.Enqueue(new Dialog(playerName, "Some universe it is, trying to get us killed right when we got here.", 4));

        //3rd wave
        thirdWaveDialogs.Enqueue(new Dialog("Ship AI", "Hostile ships detected.", 3));
        thirdWaveDialogs.Enqueue(new Dialog("Ship AI", "No more hostiles detected.", 3));
        thirdWaveDialogs.Enqueue(new Dialog("Player", "Good. Hmm, I wonder if there are any friendly humans in here.", 4));

        //4th wave
        fourthWaveDialogs.Enqueue(new Dialog("Ship AI", "Multiple unusual enemies sighted.", 3));
        fourthWaveDialogs.Enqueue(new Dialog(playerName, "What the hell? Giant eyes with tentacles? This universe is so weird!", 5));
        fourthWaveDialogs.Enqueue(new Dialog("Ship AI", "All eye monsters destroyed.", 3));
        fourthWaveDialogs.Enqueue(new Dialog(playerName, "Good, I do not want to see them ever again.", 3));

        //First boss
        bossWaveDialogs.Enqueue(new Dialog("Ship AI", "Ship with high battle power detected. Proceed with caution.", 4));
        bossWaveDialogs.Enqueue(new Dialog("Azazel", "Your soul will be mine!", 3));
        bossWaveDialogs.Enqueue(new Dialog(playerName, "My soul? What do you mean?", 3));
        bossWaveDialogs.Enqueue(new Dialog("Azazel", "Whether you realize it or not, you are already dead, and upon your death", 4));
		bossWaveDialogs.Enqueue(new Dialog("Azazel", "your soul was cast into this universe. With it, I shall escape this hell!", 4));
        bossWaveDialogs.Enqueue(new Dialog(playerName, "You will never get it! You are the one who will disapear!", 3));
        bossWaveDialogs.Enqueue(new Dialog("Azazel", "NOOOOOOOO!!!!!I WAS SO CLOSE!!!!!", 3));
    }

    private void initBossPauseDialogs()
    {
        bossPauseDialogs.Enqueue(new Dialog("Azazel", "That hardly tickles. Soon you will perish", 5));
        bossPauseDialogs.Enqueue(new Dialog("Azazel", "No, I will not lose to someone like you!", 5));
        bossPauseDialogs.Enqueue(new Dialog("Azazel", "NOOOOOOOO!!!!!I WAS SO CLOSE!!!!!", 5));
    }

    public Dialog nextDialog()
    {
        if (firstWaveDialogs.Count > 0)
        {
            return firstWaveDialogs.Dequeue();
        }
        else if (secondWaveDialogs.Count > 0)
        {
            return secondWaveDialogs.Dequeue();
        }
        else if (thirdWaveDialogs.Count > 0)
        {
            return thirdWaveDialogs.Dequeue();
        }
        else if (fourthWaveDialogs.Count > 0)
        {
            return fourthWaveDialogs.Dequeue();
        }
        else if (bossWaveDialogs.Count > 0)
        {
            return bossWaveDialogs.Dequeue();
        }
        else
        {
            return new Dialog("Game", "No more dialogs", 5);
        }
    }

    public Dialog nextBossPauseDialog()
    {
        return bossPauseDialogs.Dequeue();
    }

    public bool firstDialogsIsEmpty()
    {
        return firstWaveDialogs.Count == 0;
    }

    public bool secondDialogsIsEmpty()
    {
        return secondWaveDialogs.Count == 0;
    }

    public bool thirdDialogsIsEmpty()
    {
        return thirdWaveDialogs.Count == 0;
    }

    public bool fourthDialogsIsEmpty()
    {
        return fourthWaveDialogs.Count == 0;
    }

    public bool bossDialogsIsEmpty()
    {
        return bossWaveDialogs.Count == 0;
    }

    public int getFirstDialogsCount()
    {
        return firstWaveDialogs.Count;
    }

    public int getSecondDialogsCount()
    {
        return secondWaveDialogs.Count;
    }

    public int getThirdDialogsCount()
    {
        return thirdWaveDialogs.Count;
    }

    public int getFourthDialogsCount()
    {
        return fourthWaveDialogs.Count;
    }

    public int getBossDialogsCount()
    {
        return bossWaveDialogs.Count;
    }

    public int getBossPauseDialogsCount()
    {
        return bossPauseDialogs.Count;
    }
}
