﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialog {

    private string speaker;
    private string text;
    private float fadeOutTime;

    public Dialog(string speaker, string text, float fadeOutTime)
    {
        this.speaker = speaker;
        this.text = text;
        this.fadeOutTime = fadeOutTime;
    }

    public string getSpeaker()
    {
        return speaker;
    }

    public string getText()
    {
        return text;
    }

    public float getFadeOutTime()
    {
        return fadeOutTime;
    }
}
