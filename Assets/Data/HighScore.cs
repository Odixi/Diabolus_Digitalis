﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScore {

    public string name { get; set; }
    public int score { get; set; }

    public HighScore(string name, int score)
    {
        this.name = name;
        this.score = score;
    }

    override
    public string ToString()
    {
        return name + " " + score;
    }

}
