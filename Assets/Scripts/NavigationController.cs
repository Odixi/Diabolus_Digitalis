﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NavigationController : MonoBehaviour
{
    public Button askNameButton;
    public Button startStoryButton;
    public Button startArcadeButton;
    public Button highScoreButton;
    public Button creditsButton;
    public Button quitGameButton;
    public Button returnCreditsButton;
    public Button returnHighScoreButton;
    public Button returnAskNameButton;
    public Button deleteHighScoresButton;

    public InputField askNameField;

    public GameObject menuSoundTrack;
    public GameObject highScoreParent;
	public GameObject GameDataController;

    public float speed;

    [SerializeField]
    protected DatabaseController dbController;

    private GameObject mainCanvasObject;
    private GameObject highScoreCanvasObject;
    private GameObject creditsCanvasObject;
    private GameObject askNameCanvasObject;
    private GameObject[] scoresObjects;


    void Start()
    {
        setHighScoreBoard();

        Cursor.visible = true;

        askNameButton.GetComponent<Button>().onClick.AddListener(onAskNameButtonClicked);
        startStoryButton.GetComponent<Button>().onClick.AddListener(onStartStoryClicked);
        startArcadeButton.GetComponent<Button>().onClick.AddListener(onStartArcadeClicked);
        highScoreButton.GetComponent<Button>().onClick.AddListener(onHighScoreClicked);
        creditsButton.GetComponent<Button>().onClick.AddListener(onCreditsClicked);
        quitGameButton.GetComponent<Button>().onClick.AddListener(onQuitGameClicked);
        returnCreditsButton.GetComponent<Button>().onClick.AddListener(onReturnFromCreditsClicked);
        returnHighScoreButton.GetComponent<Button>().onClick.AddListener(onReturnFromHighScoresClicked);
        returnAskNameButton.GetComponent<Button>().onClick.AddListener(onReturnFromAskNameClicked);
        deleteHighScoresButton.GetComponent<Button>().onClick.AddListener(onDeleteHighScores);

        mainCanvasObject = GameObject.Find("MainCanvas");
        highScoreCanvasObject = GameObject.Find("HighScoreCanvas");
        creditsCanvasObject = GameObject.Find("CreditsCanvas");
        askNameCanvasObject = GameObject.Find("AskNameCanvas");

        scoresObjects = GameObject.FindGameObjectsWithTag("Scores");

        Instantiate(menuSoundTrack);

        mainCanvasObject.SetActive(true);
        highScoreCanvasObject.SetActive(false);
        creditsCanvasObject.SetActive(false);
        askNameCanvasObject.SetActive(false);

    }

    private void onDeleteHighScores()
    {
        dbController.removeAllHighscores();
        clearHighScoresFromUI();
    }

    void onAskNameButtonClicked()
    {
        mainCanvasObject.SetActive(false);
        askNameCanvasObject.SetActive(true);
        askNameField.Select();
    }

    void onStartStoryClicked()
    {
        setPlayerName();
        SceneManager.LoadScene("BeginCutscene");
        GameSessionData.mode = GameMode.STORY;
    }

    void onStartArcadeClicked()
    {
        setPlayerName();
        SceneManager.LoadScene("ArcadeMode");
        GameSessionData.mode = GameMode.ARCADE;
    }

    void setPlayerName()
    {
        if (askNameField.text.Equals(""))
        {
            PlayerPrefs.SetString("PlayerName", "Player");
        }
        else
        {
            PlayerPrefs.SetString("PlayerName", askNameField.text);
        }
    }

    void onHighScoreClicked()
    {
        mainCanvasObject.SetActive(false);
        highScoreCanvasObject.SetActive(true);
    }

    void onCreditsClicked()
    {
        //mainCanvasObject.SetActive(false);
        //creditsCanvasObject.SetActive(true);
		SceneManager.LoadScene("Credits");
    }

    void onQuitGameClicked()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }

    void onReturnFromCreditsClicked()
    {
        mainCanvasObject.SetActive(true);
        creditsCanvasObject.SetActive(false);
    }

    void onReturnFromHighScoresClicked()
    {
        mainCanvasObject.SetActive(true);
        highScoreCanvasObject.SetActive(false);
    }

    void onReturnFromAskNameClicked()
    {
        askNameCanvasObject.SetActive(false);
        mainCanvasObject.SetActive(true);
    }

    void setHighScoreBoard()
    {

        List<HighScore> highscores = dbController.getHighScores();

		for (int i = 0; i < Mathf.Min(highscores.Count, 10); i++)
        {
            Transform highScoreChild = highScoreParent.transform.GetChild(i).transform;

            highScoreChild.transform.GetChild(0).transform.GetComponent<Text>().text = "#" + (i + 1);
            highScoreChild.transform.GetChild(1).transform.GetComponent<Text>().text = highscores[i].name;
            highScoreChild.transform.GetChild(2).transform.GetComponent<Text>().text = "" + highscores[i].score;
        }
    }

    public void clearHighScoresFromUI()
    {
        for (int i = 0; i < highScoreParent.transform.childCount; i++)
        {
            Transform highScoreChild = highScoreParent.transform.GetChild(i).transform;

            highScoreChild.transform.GetChild(0).transform.GetComponent<Text>().text = "";
            highScoreChild.transform.GetChild(1).transform.GetComponent<Text>().text = "";
            highScoreChild.transform.GetChild(2).transform.GetComponent<Text>().text = "";
        }
    }
}
