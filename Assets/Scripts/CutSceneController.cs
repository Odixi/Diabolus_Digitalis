﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CutSceneController : MonoBehaviour {

    public Image img;
    public bool FadedInStart;
    public bool FadeOutEnd;
    public string NextScene;
    public GameObject DialogTextBox;
    public GameObject SpeakerTextBox;
    public float DialogStartWait;
    public float Duration;
    public float DelayOnFade;

    private GameObject dialogCanvas;
    private float Dialogdur;
    private bool start = true;
    private bool dialogStart;
    private bool DialogEnd = false;

    private GameObject player;
    private AudioSource audioSource;
    private AudioClip[] sounds;
    private bool canMove = false;

    private List<Dialog> dialogs = new List<Dialog>();
    private string playerName;

    // Use this for initialization
    void Start () {
        dialogCanvas = GameObject.Find("DialogCanvas");
        dialogCanvas.SetActive(false);

        playerName = PlayerPrefs.GetString("PlayerName");

        audioSource = GetComponent<AudioSource>();
        player = GameObject.FindGameObjectWithTag("Pelaaja");
        sounds = Resources.LoadAll<AudioClip>("CutsceneSounds");

        dialogs.Add(new Dialog(playerName, "Aaaahhh!!!", 1));
        dialogs.Add(new Dialog(playerName, "Huh, that was only a dream.", 1));
        dialogs.Add(new Dialog("Ship AI", "Warning! Hostile ships detected!", 1));
        dialogs.Add(new Dialog(playerName, " Darn It! Ship prepare battle mode!", 1));
        dialogs.Add(new Dialog("Ship AI", "Battle mode activated.", 1));
        dialogs.Add(new Dialog(playerName, " Nice, let's finish this quickly.", 1));

        Dialogdur = Duration / dialogs.Count;
    }

    void playSound(string fileName)
    {
        foreach (AudioClip clip in sounds)
        {
            if (clip.name.Equals(fileName))
            {
                audioSource.PlayOneShot(clip);
            }
        }
    }

    // Update is called once per frame
    void Update () {

        if (canMove)
        {
            player.transform.Translate(Vector3.forward);
        }
        
        if (start)
        {
            playSound("Bomb_Exploding");
            if (FadedInStart)
            {               
                StartCoroutine(FadeImage(true));
            }
            StartCoroutine(GoTroughDialog());
            start = false;
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            dialogCanvas.SetActive(false);
            StopCoroutine(GoTroughDialog());
            DialogEnd = true;
        }
        if (DialogEnd)
        {          
            if (FadeOutEnd)
            {
                StartCoroutine(FadeImage(false));
            }
            StartCoroutine(LoadNextScene());
            DialogEnd = false;
        }
	}


    IEnumerator GoTroughDialog()
    {
        yield return new WaitForSeconds(DialogStartWait);

        dialogCanvas.SetActive(true);

        foreach (Dialog dialog in dialogs)
        {
            SpeakerTextBox.GetComponent<Text>().text = dialog.getSpeaker();
            DialogTextBox.GetComponent<Text>().text = dialog.getText();
            yield return new WaitForSeconds(Dialogdur);
        }


        dialogCanvas.SetActive(false);
        DialogEnd = true;
        canMove = true;     
    }

    IEnumerator LoadNextScene()
    {
        yield return new WaitForSeconds(DelayOnFade);
        SceneManager.LoadScene(NextScene);
    }
    IEnumerator FadeImage(bool fadeAway)
    {
        // fade from opaque to transparent
        if (fadeAway)
        {
            // loop over 1 second backwards
            for (float i = DialogStartWait; i >= 0; i -= Time.deltaTime)
            {
                // set color with i as alpha
                img.color = new Color(0, 0, 0, i);
                yield return null;
            }
        }
        // fade from transparent to opaque
        else
        {
            // loop over 1 second
            for (float i = 0; i <= 1; i += Time.deltaTime)
            {
                // set color with i as alpha
                img.color = new Color(0, 0, 0, i);
                yield return null;
            }
        }
    }
}
