﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndingCutsceneController : MonoBehaviour {

	public int fadeInIn;
	public int fadeOutIn;
	public Image img;
	public int fadeInBack;
	public Transform shotSpawn;
	public GameObject shot;
	public GameObject player;
	public string portaaliTuhottu;
	public string portaaliin;
	public string poisPortaalista;
	public int LoppuTekstiRuudulla;


	public GameObject smallExplosion;
	public GameObject Portal;

	private Camera mainCam;
	private Camera playCam;
	private GameObject MainCam;
	private GameObject PlayCam;
	private GameObject textBox;
	private bool fade = true;
	private bool mn = true;
	private bool go = false;
	private bool play = false;
	private bool shotted = false;
	private bool eteen = false;
	private bool taakse = false;
	private bool ekafade = true;
	private bool loppu = false;
	public Rigidbody rb;
	private GameObject Shot;
	private Transform cube;
	private GameObject endLine;
	private string teksti;


	// Use this for initialization
	void Start () {

		endLine = GameObject.FindGameObjectWithTag("EndLine");
		textBox = GameObject.FindGameObjectWithTag ("DialogText");
		cube = Portal.GetComponent<Transform> ();
		MainCam = GameObject.FindGameObjectWithTag ("MainCamera");
		PlayCam = GameObject.FindGameObjectWithTag ("cam1");
		mainCam = MainCam.GetComponent<Camera>();
		playCam = PlayCam.GetComponent<Camera>();
		playCam.enabled = false;
		mainCam.enabled = true;
		StartCoroutine (FadeImage(fade, fadeInIn));
		StartCoroutine (FadeImage(!fade, fadeOutIn));
	}
	
	// Update is called once per frame
	void Update () {
		if(go){
			playCam.enabled = true;
			mainCam.enabled = false;
			StartCoroutine (FadeImage(fade, fadeInBack));
			go = false;
			StartCoroutine (waitForFade(fadeInBack));

		}
		if(play){
			
			float moveVertical = Input.GetAxis ("Vertical");

			if (moveVertical > 0) {
				play = false;
				Vector3 movement = new Vector3(0.0f, 0.0f, -2.0f);
				rb.velocity = movement;
				eteen = true;
			}

			if (moveVertical < 0) {
				play = false;
				Vector3 movement = new Vector3(0.0f, 0.0f, 2.0f);
				rb.velocity = movement;
				taakse = true;
			}

			if (Input.GetButton("Fire1")) {
				Shot = Instantiate(shot, shotSpawn.position, new Quaternion());
				shotted = true;
				play = false;
			}



		}
		if(shotted){
			
			if(Shot.transform.position.z < cube.position.z){
				Destroy (Shot);
				shotted = false;
				Instantiate (smallExplosion, cube.position, cube.rotation);
				Destroy (Portal);
				teksti = formatEndText(portaaliTuhottu);
				StartCoroutine (FadeImage(!fade, fadeInIn));
			}
		}
		if(eteen){
			if (player.transform.position.z < cube.position.z) {
				eteen = false;
				teksti = formatEndText(portaaliin);
				StartCoroutine (FadeImage(!fade, fadeInIn));
			}
		}

		if(taakse){
			if (player.transform.position.z > endLine.transform.position.z) {
				taakse = false;
				teksti = formatEndText(poisPortaalista);
				StartCoroutine (FadeImage(!fade, fadeInIn));
			}
		}

		if(loppu){
			loppu = false;
			img.color = new Color (0,0,0,1);
			textBox.GetComponent<Text>().text = teksti;
			StartCoroutine (end(LoppuTekstiRuudulla));
		}
	}

    string formatEndText(string text)
    {
        return string.Format(text, PlayerPrefs.GetString("PlayerName"));
    }

	void FixedUpdate(){
	}
	IEnumerator waitForFade(int n){
		yield return new WaitForSeconds (n);
		play = true;
		yield return null;
	}

	IEnumerator end(int n){
		yield return new WaitForSeconds (n);
		SceneManager.LoadScene ("Main_Menu");
		yield return null;
	}

	IEnumerator FadeImage(bool fadeAway, int n)
	{
		// fade from opaque to transparent
		if (fadeAway)
		{
			// loop over 1 second backwards
			for (float i = n; i >= 0; i -= Time.deltaTime)
			{
				// set color with i as alpha
				img.color = new Color(0, 0, 0, i);
				yield return null;
			}
		}
		// fade from transparent to opaque
		else
		{
			
			if (mn) {
				yield return new WaitForSeconds (n);
				mn = false;
			}
			// loop over 1 second
			for (float i = 0; i <= 1; i += Time.deltaTime)
			{
				// set color with i as alpha
				if(ekafade){
				img.color = new Color(0, 0, 0, i);
				}
				if(!ekafade){
					img.color = new Color(255, 255, 255, i);
				}
				if(i >= 0.99f && !ekafade){
					loppu = true;
				}
				if(i >= 0.99f && ekafade){
					ekafade = false;
					go = true;
				}



				yield return null;
			}
			SceneManager.LoadScene("FinalScene");
		}
	}
}
