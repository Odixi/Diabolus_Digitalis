﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using System;

public class EnemySpawner : MonoBehaviour
{
    private enum EnemyType
    {
        MOVING,
        STATIC,
        WINDING,
        PARABOLA,
        PARABOLA_BEHIND,
        BEND,
        SWIPE,
        KAMIKAZE,
        EYE_MONSTER,
		LARGE1,
		LARGE2,
		SQUAD,
		FORMATION,
		STRAIGHT,
        BOSS_CRUISER,
        BOSS_TRANSFORMER
    }

    public GameObject   enemyBendCurve, enemyCircleCurve, enemyCircleSpinCurve, enemyCruiser, enemyEye, enemyEyeStory, enemyFormation, 
                        enemyKamikaze, enemyParabolaCurve, enemyLarge, enemyLarge2, enemyParabolaBehindCurve, enemyShotStorm, 
                        enemyStraight, enemySwipeCurve, enemyWindingCurve, enemyZigZagCurve, enemyZigZag2, enemyTransformer, enemySquad, 
                        enemySpiral;

    public GameObject   curveCircle, curveCircleSpin, curveCruiser, curveFormationLeft, curveFormationRight, curveLargeEnemyLeft, 
                        curveLargeEnemyRight, curveLeftToRight, curveRightToLeft, curveParabolaLeftToRight, curveParabolaRightToLeft,
                        curveParabolaBehindLeftToRight, curveParabolaBehindRightToLeft, curveShotStorm, curveSpiral, curveSquadLeft, 
                        curveSquadRight, curveStraightLeftToRight, curveStraightRightToLeft, curveSwipeLeftToRight, 
                        curveSwipeRightToLeft, curveWinding, curveZigZag, curveZigZag2;

    public float        spawnWait;

    private bool        bossIsAlive;
    private Vector3     spawnValues, spawnPosition;
    private GameObject  boss;
    private GameObject  enemyInstance;
    private GameObject  curveInstance;

    // Some variables for balancing the game
    public int          startingWaveMaxiumScore;
    public float        frequencyOfRandomWaits;
    public float        minDurationOfRandomWaits;
    public float        maxDurationOfRandomWaits;
    public float        enemySpawnFrequency;
    // spawns per second
    public float        enemySpawnFrequencyMultipler;
    public float        scorePerWaveIncreaseMultipler;
    public float        enemyDamageIncreaseMultipler;
    public float        enemyHealthIncreaseMultipler;

    private GameObject  gameController;

    private int         windingEnemiesPerSpawn = 4;

    void Start()
    {
        gameController = GameObject.Find("GameController");
    }

    /*
	 * Once the enemies of the round have been placed in the stack, pop and spawn them
	 */
    public IEnumerator SpawnArcadeRound(int roundNumber)
    {
        windingEnemiesPerSpawn = roundNumber >= 10 ? 8 : (int)(4 + (float)roundNumber * (6.0 / 10.0));

        // Boss?
        if (roundNumber % 5 != 0)
        {
            Stack enemyStack = enemiesOnThisWave();
            EnemyType enemyType;
            float spawnWait;
			int tempEnemyAmount;

            while (enemyStack.Count > 0 && gameController.GetComponent<GameController>().state == GameController.GameState.PLAY)
            {
                // Random taukoja
                if (UnityEngine.Random.value > frequencyOfRandomWaits)
                {
                    yield return new WaitForSeconds(minDurationOfRandomWaits + UnityEngine.Random.value *
                        (maxDurationOfRandomWaits - minDurationOfRandomWaits));
                }

                enemyType = (EnemyType)enemyStack.Pop(); // Let's take the new enemy
                spawnWait = 1 / (enemySpawnFrequency * Mathf.Pow(enemySpawnFrequencyMultipler, roundNumber - 1));

                switch (enemyType)
                {
                    case (EnemyType.MOVING):
                        yield return StartCoroutine(SpawnEnemy(enemyCircleCurve, curveCircle, spawnWait, 1));
                        break;

                    case (EnemyType.STATIC):
                        yield return StartCoroutine(SpawnEnemy(enemyZigZagCurve, curveZigZag, spawnWait, 1));
                        break;

                    case (EnemyType.WINDING):
                        yield return StartCoroutine(SpawnEnemy(enemyWindingCurve, curveWinding, 0.15f, windingEnemiesPerSpawn)); // !!!
                        break;

                    case (EnemyType.PARABOLA):
                        if (UnityEngine.Random.value > 0.5f)
                        {
                            yield return StartCoroutine(SpawnEnemy(enemyParabolaCurve, curveParabolaLeftToRight, spawnWait, 1));
                        }
                        else
                        {
                            yield return StartCoroutine(SpawnEnemy(enemyParabolaCurve, curveParabolaRightToLeft, spawnWait, 1));
                        }
                        break;

                    case (EnemyType.PARABOLA_BEHIND):
                        if (UnityEngine.Random.value > 0.5f)
                        {
                            yield return StartCoroutine(SpawnEnemy(enemyParabolaBehindCurve, curveParabolaBehindRightToLeft, spawnWait, 1));
                        }
                        else
                        {
                            yield return StartCoroutine(SpawnEnemy(enemyParabolaBehindCurve, curveParabolaBehindLeftToRight, spawnWait, 1));
                        }
                        break;

                    case (EnemyType.BEND):
                        if (UnityEngine.Random.value > 0.5f)
                        {
                            yield return StartCoroutine(SpawnEnemy(enemyBendCurve, curveRightToLeft, 0.2f, 4));
                        }
                        else
                        {
                            yield return StartCoroutine(SpawnEnemy(enemyBendCurve, curveLeftToRight, 0.2f, 4));
                        }
                        break;

                    case (EnemyType.SWIPE):
                        if (UnityEngine.Random.value > 0.5f)
                        {
                            yield return StartCoroutine(SpawnEnemy(enemySwipeCurve, curveSwipeRightToLeft, spawnWait, 1));
                        }
                        else
                        {
                            yield return StartCoroutine(SpawnEnemy(enemySwipeCurve, curveSwipeLeftToRight, spawnWait, 1));
                        }
                        break;

                    case (EnemyType.KAMIKAZE):
                        yield return StartCoroutine(SpawnEnemy(enemyKamikaze, spawnWait, 1));
                        break;

                    case (EnemyType.EYE_MONSTER):
                        yield return StartCoroutine(SpawnEnemy(enemyEye, spawnWait, 1));
                        break;

					case (EnemyType.LARGE1):
						if (UnityEngine.Random.value > 0.5f) {
							yield return StartCoroutine (SpawnEnemy (enemyLarge, curveLargeEnemyLeft, spawnWait, 1));
						} else {
							yield return StartCoroutine (SpawnEnemy (enemyLarge, curveLargeEnemyRight, spawnWait, 1));
						}
						yield return new WaitForSeconds (3);
						break;

					case (EnemyType.LARGE2):
						if (UnityEngine.Random.value > 0.5f)
						{
							yield return StartCoroutine(SpawnEnemy(enemyLarge2, curveLargeEnemyLeft, spawnWait, 1));
						}
						else
						{
							yield return StartCoroutine(SpawnEnemy(enemyLarge2, curveLargeEnemyRight, spawnWait, 1));
						}
						yield return new WaitForSeconds (3);
						break;

					case (EnemyType.FORMATION):
						if (UnityEngine.Random.value > 0.5f)
						{
							yield return StartCoroutine(SpawnEnemyAlternating(enemyFormation, curveFormationLeft, curveFormationRight, 0, 6));
						}
						else
						{
							yield return StartCoroutine(SpawnEnemyAlternating(enemyFormation, curveFormationRight, curveFormationLeft, 0, 6));
						}
						break;

				case (EnemyType.STRAIGHT):
					tempEnemyAmount = 1;
					while (enemyStack.Count > 0 && (EnemyType)enemyStack.Peek () == EnemyType.STRAIGHT) {
						enemyStack.Pop ();
						tempEnemyAmount++;
					}
						if (UnityEngine.Random.value > 0.5f)
						{
						yield return StartCoroutine(SpawnEnemy(enemyStraight, curveStraightLeftToRight, 0, tempEnemyAmount));
						}
						else
						{
						yield return StartCoroutine(SpawnEnemy(enemyStraight, curveStraightRightToLeft, 0, tempEnemyAmount));
						}
						break;

				case (EnemyType.SQUAD):
					tempEnemyAmount = 1;
					while (enemyStack.Count > 0 && (EnemyType)enemyStack.Peek () == EnemyType.SQUAD) {
						enemyStack.Pop ();
						tempEnemyAmount++;
					}

					yield return StartCoroutine (SpawnEnemyAlternating (enemySquad, curveSquadLeft, curveSquadRight, 0.2f, tempEnemyAmount));
					yield return new WaitForSeconds (0.3f * tempEnemyAmount);
					break;

                }//switch
            }// end while
        }
        else
        { // bossi
			if (roundNumber % 10 == 0) {
				yield return StartCoroutine (SpawnEnemy (enemyCruiser, curveCruiser, spawnWait, 1));
			} else {
				yield return StartCoroutine(SpawnEnemy(enemyTransformer, spawnWait, 1));
			}
            bossIsAlive = true;
            while (bossIsAlive)
            {
                yield return new WaitForSeconds(2);
                bossIsAlive = boss == null ? false : true;
            }
        }
    }

    // This is just a test for now
    public IEnumerator SpawnStoryModeRound(int roundNumber)
    {
        switch (roundNumber) 
        {
            case 1:
                yield return StartCoroutine(SpawnEnemy(enemyBendCurve, curveRightToLeft, 0.2f, 5));
                yield return new WaitForSeconds(1);
                yield return StartCoroutine(SpawnEnemy(enemyBendCurve, curveLeftToRight, 0.2f, 5));
                yield return StartCoroutine(SpawnEnemyAlternating(enemyLarge, curveLargeEnemyLeft, curveLargeEnemyRight, 0.0f, 2));
                yield return new WaitForSeconds(2);
                yield return StartCoroutine(SpawnEnemyAlternating(enemyParabolaBehindCurve, curveParabolaBehindLeftToRight, curveParabolaBehindRightToLeft, 1.5f, 2));
                yield return new WaitForSeconds(1);
                yield return StartCoroutine(SpawnEnemy(enemyBendCurve, curveLeftToRight, 0.2f, 5));
                yield return new WaitForSeconds(1);
                yield return StartCoroutine(SpawnEnemy(enemyBendCurve, curveRightToLeft, 0.2f, 5));
                yield return new WaitForSeconds(1);
                yield return StartCoroutine(SpawnEnemyAlternating(enemyParabolaCurve, curveParabolaLeftToRight, curveParabolaRightToLeft, 1f, 2));
                yield return StartCoroutine(SpawnEnemyAlternating(enemyFormation, curveFormationLeft, curveFormationRight, 0.0f, 6));
                yield return new WaitForSeconds(1.5f);
                yield return StartCoroutine(SpawnEnemyAlternating(enemyFormation, curveFormationRight, curveFormationLeft, 0.0f, 6));
                yield return new WaitForSeconds(2.3f);
                yield return StartCoroutine(SpawnEnemy(enemyCircleSpinCurve, curveCircleSpin, 0.0f, 8));
                yield return new WaitForSeconds(13);
                yield return StartCoroutine(SpawnEnemyAlternating(enemySwipeCurve, curveSwipeRightToLeft, curveSwipeLeftToRight, 0.5f, 2));
                yield return new WaitForSeconds(3);
                yield return StartCoroutine(SpawnEnemyAlternating(enemySquad, curveSquadLeft, curveSquadRight, 0.2f, 8));
                break;
            case 2:
                yield return StartCoroutine(SpawnEnemy(enemySpiral, curveSpiral, 0.15f, 16));
                yield return new WaitForSeconds(2);
                yield return StartCoroutine(SpawnEnemyAlternating(enemySquad, curveSquadLeft, curveSquadRight, 0.1f, 16));
                yield return new WaitForSeconds(4);
                yield return StartCoroutine(SpawnEnemyAlternating(enemyFormation, curveFormationRight, curveFormationLeft, 0.0f, 6));
                yield return new WaitForSeconds(2.5f);
                yield return StartCoroutine(SpawnEnemy(enemyStraight, curveStraightLeftToRight, 0.0f, 3));
                yield return new WaitForSeconds(1);
                yield return StartCoroutine(SpawnEnemy(enemyStraight, curveStraightRightToLeft, 0.0f, 3));
                yield return new WaitForSeconds(0.5f);
                yield return StartCoroutine(SpawnEnemyAlternating(enemySwipeCurve, curveSwipeRightToLeft, curveSwipeLeftToRight, 0.5f, 2));
                yield return new WaitForSeconds(1);
                yield return StartCoroutine(SpawnEnemyAlternating(enemyLarge2, curveLargeEnemyLeft, curveLargeEnemyRight, 0.0f, 2));
                yield return new WaitForSeconds(5);
                yield return StartCoroutine(SpawnEnemy(enemyStraight, curveStraightRightToLeft, 0.0f, 1));
                yield return StartCoroutine(SpawnEnemy(enemyStraight, curveStraightLeftToRight, 0.0f, 1));
                yield return new WaitForSeconds(1);
                yield return StartCoroutine(SpawnEnemyAlternating(enemyBendCurve, curveRightToLeft, curveLeftToRight, 0.5f, 6));
                yield return StartCoroutine(SpawnEnemyAlternating(enemyFormation, curveFormationLeft, curveFormationRight, 0.0f, 6));
                yield return new WaitForSeconds(1.5f);
                yield return StartCoroutine(SpawnEnemyAlternating(enemyZigZag2, curveZigZag, curveZigZag2, 0.0f, 2));
                yield return new WaitForSeconds(2);
                yield return StartCoroutine(SpawnEnemy(enemyStraight, curveStraightRightToLeft, 0.0f, 3));
                yield return new WaitForSeconds(1);
                yield return StartCoroutine(SpawnEnemy(enemyStraight, curveStraightLeftToRight, 0.0f, 3));
                yield return new WaitForSeconds(0.5f);
                yield return StartCoroutine(SpawnEnemyAlternating(enemyFormation, curveFormationLeft, curveFormationRight, 0.0f, 6));
                break;
            case 3:
                yield return StartCoroutine(SpawnEnemyAlternating(enemyZigZag2, curveZigZag, curveZigZag2, 0.0f, 2));
                yield return new WaitForSeconds(1);
                yield return StartCoroutine(SpawnEnemyAlternating(enemyFormation, curveFormationRight, curveFormationLeft, 0.0f, 6));
                yield return new WaitForSeconds(1);
                yield return StartCoroutine(SpawnEnemyAlternating(enemySwipeCurve, curveSwipeRightToLeft, curveSwipeLeftToRight, 0.5f, 2));
                yield return new WaitForSeconds(2);
                yield return StartCoroutine(SpawnEnemy(enemyCircleSpinCurve, curveCircleSpin, 0.0f, 8));
                yield return new WaitForSeconds(13);
                yield return StartCoroutine(SpawnEnemyAlternating(enemyParabolaBehindCurve, curveParabolaBehindLeftToRight, curveParabolaBehindRightToLeft, 1.5f, 2));
                yield return new WaitForSeconds(0.2f);
                yield return StartCoroutine(SpawnEnemy(enemyParabolaCurve, curveParabolaRightToLeft, 0.5f, 4));
                yield return new WaitForSeconds(0.5f);
                yield return StartCoroutine(SpawnEnemy(enemyParabolaCurve, curveParabolaLeftToRight, 0.5f, 4));
                yield return StartCoroutine(SpawnEnemy(enemyBendCurve, curveRightToLeft, 0.2f, 5));
                yield return StartCoroutine(SpawnEnemy(enemyBendCurve, curveLeftToRight, 0.2f, 5));
                yield return new WaitForSeconds(1);
                yield return StartCoroutine(SpawnEnemyAlternating(enemySquad, curveSquadLeft, curveSquadRight, 0.1f, 16));
                yield return new WaitForSeconds(3);
                yield return StartCoroutine(SpawnEnemy(enemyShotStorm, curveShotStorm, 0.0f, 1));
                yield return new WaitForSeconds(16);
                break;
            case 4:
                yield return StartCoroutine(SpawnEnemy(enemyEyeStory, spawnWait, 1));
                yield return new WaitForSeconds(3);
                yield return StartCoroutine(SpawnEnemy(enemyParabolaBehindCurve, curveParabolaBehindRightToLeft, 0.5f, 4));
                yield return new WaitForSeconds(1);
                yield return StartCoroutine(SpawnEnemy(enemyParabolaBehindCurve, curveParabolaBehindLeftToRight, 0.5f, 4));
                yield return new WaitForSeconds(0.5f);
                yield return StartCoroutine(SpawnEnemy(enemyEyeStory, 0, 1));
                yield return new WaitForSeconds(1);
                yield return StartCoroutine(SpawnEnemy(enemyEyeStory, 0, 1));
                yield return new WaitForSeconds(3);
                yield return StartCoroutine(SpawnEnemy(enemySpiral, curveSpiral, 0.15f, 16));
                yield return new WaitForSeconds(3);
                yield return StartCoroutine(SpawnEnemy(enemyEyeStory, 1, 3));
                yield return new WaitForSeconds(3);
                yield return StartCoroutine(SpawnEnemyAlternating(enemyLarge, curveLargeEnemyLeft, curveLargeEnemyRight, 0.0f, 2));
                yield return new WaitForSeconds(7);
                yield return StartCoroutine(SpawnEnemy(enemyEyeStory, 1, 5));
                break;
		case 5:
			yield return StartCoroutine (SpawnEnemy (enemyTransformer, spawnWait, 1));
			bossIsAlive = true;
			while (bossIsAlive) {
				yield return new WaitForSeconds (2);
				bossIsAlive = boss == null ? false : true;
			}
			yield return new WaitForSeconds (5);
			GameSessionData.gameOver = true;
                SceneManager.LoadScene("EndingCutscene");
                break;
		}
			
    }

    // For testing scene
    public IEnumerator SpawnTestRound(int roundNumber)
    {
        yield return new WaitForSeconds(1);
		yield return StartCoroutine(SpawnEnemy(enemyTransformer, spawnWait, 1));
        while (true)
        {
            yield return new WaitForSeconds(1);
        }
    }

    /*
    * Täytetään stacki vihollisilla, haluttuun pistemäärään asti
    *
    */
    private Stack enemiesOnThisWave()
    {
        Stack enemyStack = new Stack();

        int listsTotalScore = 0;
        var types = Enum.GetValues(typeof(EnemyType));
        EnemyType randomType;
		int temp = 0;

        while (listsTotalScore < startingWaveMaxiumScore * Mathf.Pow(scorePerWaveIncreaseMultipler, GameSessionData.wave - 1))
        {
            randomType = (EnemyType)types.GetValue(UnityEngine.Random.Range(0, types.Length));

            switch (randomType)
            {

                case ( EnemyType.MOVING):
                    listsTotalScore += enemyCircleCurve.GetComponent<EntityUserData>().points;
                    enemyStack.Push(EnemyType.MOVING);
                    break;

                case (EnemyType.STATIC):
                    listsTotalScore += enemyZigZagCurve.GetComponent<EntityUserData>().points;
                    enemyStack.Push(EnemyType.STATIC);
                    break;

                case (EnemyType.WINDING):
                    if (UnityEngine.Random.value > 0.9f)
                    {// Tehdään vähän harvinaisemmiksi kuin muut
                        listsTotalScore += enemyWindingCurve.GetComponent<EntityUserData>().points * windingEnemiesPerSpawn;
                        enemyStack.Push(EnemyType.WINDING);
                    }
                    break;

                case (EnemyType.PARABOLA):
                    listsTotalScore += enemyParabolaCurve.GetComponent<EntityUserData>().points;
                    enemyStack.Push(EnemyType.PARABOLA);
                    break;

                case (EnemyType.PARABOLA_BEHIND):
                    listsTotalScore += enemyParabolaBehindCurve.GetComponent<EntityUserData>().points;
                    enemyStack.Push(EnemyType.PARABOLA_BEHIND);
                    break;

                case (EnemyType.BEND):
                    if (UnityEngine.Random.value > 0.75f)
                    { // Hieman harvinaisempi
                        listsTotalScore += enemyBendCurve.GetComponent<EntityUserData>().points * 4;
                        enemyStack.Push(EnemyType.BEND);
                    }
                    break;
                case (EnemyType.SWIPE):
                    listsTotalScore += enemySwipeCurve.GetComponent<EntityUserData>().points;
                    enemyStack.Push(EnemyType.SWIPE);
                    break;

                case (EnemyType.KAMIKAZE):
                    listsTotalScore += enemyKamikaze.GetComponent<EntityUserData>().points;
                    enemyStack.Push(EnemyType.KAMIKAZE);
                    break;
                case (EnemyType.EYE_MONSTER):
                    if (UnityEngine.Random.value > 0.6f)
                    {
                        listsTotalScore += enemyEye.GetComponent<EntityUserData>().points;
                        enemyStack.Push(EnemyType.EYE_MONSTER);
                    }
                    break;
				case (EnemyType.LARGE1):
					if (UnityEngine.Random.value > 0.9f) {
						listsTotalScore += enemyLarge.GetComponent<EntityUserData> ().points;
						enemyStack.Push (EnemyType.LARGE1);
					}
					break;
				case(EnemyType.LARGE2):
					if (UnityEngine.Random.value > 0.9f) {
						listsTotalScore += enemyLarge2.GetComponent<EntityUserData> ().points;
						enemyStack.Push (EnemyType.LARGE2);
					}
					break;
			case(EnemyType.FORMATION):
				if (UnityEngine.Random.value > 0.85f) {
					listsTotalScore += enemyFormation.GetComponent<EntityUserData> ().points * 6;
					enemyStack.Push (EnemyType.FORMATION);
				}
						break;
			case(EnemyType.STRAIGHT):
				if (UnityEngine.Random.value > 0.4f) {
					temp = UnityEngine.Random.Range (1, 4);
					listsTotalScore += temp * enemyStraight.GetComponent<EntityUserData> ().points;
					for (int i = 0; i < temp; i++) {
						enemyStack.Push (EnemyType.STRAIGHT);
					}
				}
					break;
			case(EnemyType.SQUAD):
				if (UnityEngine.Random.value > 0.6f) {
					temp = UnityEngine.Random.Range (2, 9);
					listsTotalScore += enemySquad.GetComponent<EntityUserData> ().points;
					for (int i = 0; i < temp; i++) {
						enemyStack.Push (EnemyType.SQUAD);
					}
				}
					break;

            }//switch
        }//while
        return enemyStack;
    }

    /*
     * SpawnEnemy() takes as its arguments the enemy GameObject, optionally the curve the enemy will use, 
     * the time delay between spawning each enemy and the number of enemies to be spawned.
     */
    IEnumerator SpawnEnemy(GameObject enemy, GameObject bezierCurve, float spawnWait, int enemyCount)
    {

        for (int i = 0; i < enemyCount; i++)
        {
			// Let's not edit the prefab, but the instance instead -Ville

            EntityUserData enemyData = enemy.GetComponent<EntityUserData>();

			// if enemyCount is begger than spanwValue count, then rotate it. 
			int j = i;
			while (j >= enemyData.spawnValues.Count) {
				j -= enemyData.spawnValues.Count;
			}

			/*
            // If spawnValues or spawnRotations has too few speeds listed, the required amount is automatically added.
            if (enemyData.spawnValues.Count <= i)
            {
                enemyData.spawnValues.Add(enemyData.spawnValues[0]);
            }
            if (enemyData.spawnRotations.Count <= i)
            {
                enemyData.spawnRotations.Add(enemyData.spawnRotations[0]);
            }
			*/

            Vector3 spawnRotation = enemyData.spawnRotations[j];

            if (enemyData.randomizeSpawnValues == true)
            {
                spawnPosition = new Vector3(UnityEngine.Random.Range(enemyData.spawnValues[j].x, -enemyData.spawnValues[j].x), 
                    enemyData.spawnValues[j].y, enemyData.spawnValues[j].z);
            }
            else
            {
                spawnPosition = enemyData.spawnValues[j];
            }

            enemyInstance = Instantiate(enemy, spawnPosition, Quaternion.Euler(spawnRotation.x, spawnRotation.y, spawnRotation.z));
            curveInstance = Instantiate(bezierCurve, spawnPosition, Quaternion.Euler(spawnRotation.x, spawnRotation.y, spawnRotation.z));
            enemyInstance.GetComponent<CurveMover>().movementCurve = curveInstance.GetComponent<BezierCurve>();
			EntityUserData eudInstance = enemyInstance.GetComponent<EntityUserData> ();
			if (GameSessionData.mode != GameMode.STORY) {
				eudInstance.damage = (int)(enemyData.damage * Mathf.Pow (enemyDamageIncreaseMultipler, GameSessionData.wave));
				eudInstance.health = (int)(enemyData.health * Mathf.Pow (enemyHealthIncreaseMultipler, GameSessionData.wave));
			} else {
				//eudInstance.dropsResource = GameSessionData.mode != GameMode.STORY ? true : false;
                eudInstance.dropsResource = true;
				enemyInstance.GetComponent<DestroyByContact>().setCanDropResource(enemyData.dropsResource);
			}
			if (enemyInstance.GetComponent<BossWeaponController>() != null || enemyInstance.tag == "BossTransformer")
            {
                boss = enemyInstance;
            }
            yield return new WaitForSeconds(spawnWait);
        }
    }

    // Alternatingly spawn an enemy with different curves
    IEnumerator SpawnEnemyAlternating(GameObject enemy, GameObject bezierCurve0, GameObject bezierCurve1, float spawnWait, int enemyCount)
    {

        for (int i = 0; i < enemyCount; i++)
        {
            EntityUserData enemyData = enemy.GetComponent<EntityUserData>();

			int j = i;
			while (j >= enemyData.spawnValues.Count) {
				j -= enemyData.spawnValues.Count;
			}

			/*
            // If spawnValues or spawnRotations has too few speeds listed, the required amount is automatically added.
            if (enemyData.spawnValues.Count <= i)
            {
                enemyData.spawnValues.Add(enemyData.spawnValues[0]);
            }
            if (enemyData.spawnRotations.Count <= i)
            {
                enemyData.spawnRotations.Add(enemyData.spawnRotations[0]);
            }
			*/

            Vector3 spawnRotation = enemyData.spawnRotations[j];

            spawnPosition = enemyData.spawnValues[j];
            enemyInstance = Instantiate(enemy, spawnPosition, Quaternion.Euler(spawnRotation.x, spawnRotation.y, spawnRotation.z));

            if (i % 2 == 0) 
            {
                curveInstance = Instantiate(bezierCurve1, spawnPosition, Quaternion.Euler(spawnRotation.x, spawnRotation.y, spawnRotation.z));
            }
            else
            {
                curveInstance = Instantiate(bezierCurve0, spawnPosition, Quaternion.Euler(spawnRotation.x, spawnRotation.y, spawnRotation.z));
            }
            enemyInstance.GetComponent<CurveMover>().movementCurve = curveInstance.GetComponent<BezierCurve>();

			EntityUserData eudInstance = enemyInstance.GetComponent<EntityUserData> ();
			if (GameSessionData.mode != GameMode.STORY) {
				eudInstance.damage = (int)(enemyData.damage * Mathf.Pow (enemyDamageIncreaseMultipler, GameSessionData.wave));
				eudInstance.health = (int)(enemyData.health * Mathf.Pow (enemyHealthIncreaseMultipler, GameSessionData.wave));
			} else {
				//eudInstance.dropsResource = GameSessionData.mode != GameMode.STORY ? true : false;
                eudInstance.dropsResource = true;
				enemyInstance.GetComponent<DestroyByContact>().setCanDropResource(enemyData.dropsResource);
			}

			if (enemyInstance.GetComponent<BossWeaponController>() != null || enemyInstance.tag == "BossTransformer")
            {
                boss = enemyInstance;
            }
            yield return new WaitForSeconds(spawnWait);
        }
    }

    IEnumerator SpawnEnemy(GameObject enemy, float spawnWait, int enemyCount)
    {

        for (int i = 0; i < enemyCount; i++)
        {
            EntityUserData enemyData = enemy.GetComponent<EntityUserData>();

			int j = i;
			while (j >= enemyData.spawnValues.Count) {
				j -= enemyData.spawnValues.Count;
			}

            Quaternion spawnRotation = Quaternion.identity;

			/*
            // If spawnValues has too few speeds listed, the required amount is automatically added.
            if (enemyData.spawnValues.Count <= i)
            {
                enemyData.spawnValues.Add(enemyData.spawnValues[0]);
            }
			*/

            if (enemyData.randomizeSpawnValues == true)
            {
                spawnPosition = new Vector3(UnityEngine.Random.Range(enemyData.spawnValues[j].x, -enemyData.spawnValues[j].x), 
                    enemyData.spawnValues[j].y, enemyData.spawnValues[j].z);
            }
            else
            {
                spawnPosition = enemyData.spawnValues[j];
            }

            enemyInstance = Instantiate(enemy, spawnPosition, spawnRotation);
			EntityUserData eudInstance = enemyInstance.GetComponent<EntityUserData> ();
			if (GameSessionData.mode != GameMode.STORY) {
				eudInstance.damage = (int)(enemyData.damage * Mathf.Pow (enemyDamageIncreaseMultipler, GameSessionData.wave));
				eudInstance.health = (int)(enemyData.health * Mathf.Pow (enemyHealthIncreaseMultipler, GameSessionData.wave));
			} else {
				//eudInstance.dropsResource = GameSessionData.mode != GameMode.STORY ? true : false;
                eudInstance.dropsResource = true;
				enemyInstance.GetComponent<DestroyByContact>().setCanDropResource(enemyData.dropsResource);
			}
			if (enemyInstance.GetComponent<BossWeaponController>() != null || enemyInstance.tag == "BossTransformer")
			{
				boss = enemyInstance;
				if (GameSessionData.isStoryMode ()) {
					gameController.GetComponent<GameController> ().startPaceBossDialogs (boss);
				}
			}
            yield return new WaitForSeconds(spawnWait);
        }
    }
}