﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FInalSceneController : MonoBehaviour {

	public Image image;
	public float fadeInTime;
	public float fadeOutTime;
	public AnimationCurve explosionTime;
	public float explosionDuration;
	public GameObject explosion;
	public GameObject explosion2;
	public float explosionSize;
	public float waitTimeAfterDecision;
	public float waitTimeUntilSceneChange;
	public float playerMovementspeed;
	public float playerShrinkspeed;

	private int stateIndex;
	private GameObject portal;
	private GameObject player;
	private float tempTime;

	private string textPortalDestroyed = "And so the hell universe Diabolus Digitalis came to " +
	"its end. Erased from existence, its remaining dwellers died their final death. Souls bound " +
	"to the realm of digitalis were freed and they were able to find peace. Ghosts from the normal universe " +
	"disappeared peacefully. Knowing all this {0}’s soul was free from guilt and {0} embraced the " +
	"explosion that would forgive their sins.";
	
	private string textPortalEntered = "And so {0} was able to return to their own universe. {0} recovered their original" +
	" body and felt fully alive again. However, the souls of the dead did not forget how {0} was responsible for their " +
	"deaths. {0} would thereafter be haunted by these ghosts that would never grant forgiveness or peace. " +
	"Eventually, {0} could not take it anymore…";
	
	private string textPortalLeftUntotched = "And so {0} embraced their fate as the ruler of Diabolus Digitalis. {0} would continue " +
	"to destroy other Digitalis dwellers to gain ultimate power. {0} would be immortal and unstoppable. Whenever someone would end up " +
	"in Diabolus Digitalis, {0} would be there to reap their soul.";

	private Text textBox;

	void Start () {
		stateIndex = 0;
		image.color = new Color(0,0,0,1f);
		player = GameObject.Find ("Player");
		player.GetComponent<PlayerController> ().enabled = false;
		textBox = GameObject.Find ("Text").GetComponent<Text>();
		textBox.color = new Color (1f, 1f, 1f, 0);

	}
	
	// Update is called once per frame
	void Update () {
		switch (stateIndex) {

		case 0: // Fade in
			image.color = image.color.a < 0 ? new Color (0, 0, 0, 0) : new Color (0, 0, 0, image.color.a - fadeInTime * Time.deltaTime);
			stateIndex = image.color.a >= 0 ? 0 : -1;
			if (stateIndex == -1) {
				player.GetComponent<PlayerController>().enabled = true;
			}
			break;

		case 1: // Portal explosion
			Time.timeScale = explosionTime.Evaluate (tempTime);
			tempTime += Time.deltaTime * explosionDuration;
			stateIndex = waitTimeAfterDecision > tempTime / explosionDuration ? 1 : 4;
			break;

		case 2: // Player enters Portal
			tempTime += Time.deltaTime;
			if (player != null) {
				player.transform.position += Vector3.forward * playerMovementspeed;
				player.transform.localScale += Vector3.forward * playerShrinkspeed;
				if (player.transform.localScale.z <= 0.05f) {
					Destroy (player);
				}
			}
			if (tempTime > waitTimeAfterDecision) {
				stateIndex = 4;
			}
			break;

		case 3: // player goes away
			tempTime += Time.deltaTime;
			if (player != null) {
				player.transform.position += Vector3.forward * playerMovementspeed * 10f;
			}
				if (tempTime > waitTimeAfterDecision) {
					stateIndex = 4;
				}

			break;

		case 4: // Fade out
			image.color = image.color.a > 1 ? new Color (0, 0, 0, 1f) : new Color (0, 0, 0, image.color.a + fadeOutTime * Time.deltaTime);
			if (image.color.a > 0.6f) {
				textBox.color = textBox.color.a > 1 ? new Color (1f, 1f, 1f, 1f) : new Color (1f, 1f, 1f, textBox.color.a + fadeOutTime * Time.deltaTime);
			}
			if (textBox.color.a >= 1) {
				if (player != null) {
					player.GetComponent<PlayerController> ().enabled = false;
				}
				stateIndex = 5;
				tempTime = 0;
			}
			break;

		case 5: // Wait 
			tempTime += Time.deltaTime;
			if (tempTime > waitTimeUntilSceneChange || Input.GetKeyDown (KeyCode.Escape)) {
				SceneManager.LoadScene ("Credits");
			}
			break;
		}


	}

	public void PortalDestroyed(GameObject portal){
		if (stateIndex == -1) {
			this.portal = portal;
			Destroy (portal);
			stateIndex = 1;
			GameObject expInst = Instantiate (explosion, transform.position, new Quaternion());
			Quaternion rot = new Quaternion();
			rot.SetLookRotation (Vector3.up);
			Instantiate (explosion2, transform.position, rot);
			expInst.transform.localScale = new Vector3 (1, 1, 1) * explosionSize;
			tempTime = 0;

			textBox.text = string.Format(textPortalDestroyed, PlayerPrefs.GetString("PlayerName"));
		}
	}

	public void PortalEntered(GameObject player){
		if (stateIndex == -1) {
			this.player = player;
			player.GetComponent<PlayerController>().enabled = false;
			player.GetComponent<Rigidbody> ().velocity = Vector3.zero;
			textBox.text = string.Format(textPortalEntered, PlayerPrefs.GetString("PlayerName"));
			tempTime = 0;
			stateIndex = 2;
		}
	}

	public void PortalLeftUntouched(GameObject player){
		if (stateIndex == -1) {
			this.player = player;
			player.GetComponent<PlayerController>().enabled = false;
			player.GetComponent<Rigidbody> ().velocity = Vector3.zero;
			textBox.text = string.Format(textPortalLeftUntotched, PlayerPrefs.GetString("PlayerName"));
			tempTime = 0;
			stateIndex = 3;
		}
	}
}
