﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformerBullet2Mover : MonoBehaviour {

	private Rigidbody rb;
	public float acceleration = 1.001f;
	public float startSpeed = 1f;
	public bool move = false;

	void Start(){
		rb = GetComponent<Rigidbody> ();
	}

	void FixedUpdate () {
		if (move) {
			rb.velocity = rb.velocity * acceleration;
		}	
	}

	public void Move(){
		Vector3 direction = new Vector3(Random.Range(-1f,1f), 0 ,Random.Range(-1f,1f)).normalized;
		rb.velocity = (direction * (startSpeed*Random.value + 0.5f));
		move = true;
	}
}
