﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformerMover : MonoBehaviour {

	private Animator animator;

	private int animIndex; // what aimation to play
	private bool shipMode = true; // flag for animator
	private float animSpeed; // Use this to run animations backwards
	private bool begining = true;
	private GameObject player;
	private Rigidbody rb;
	private Vector3 defPosition;
	private Vector3 pointToLookAt;

	private int currentAttackPattern = 0;
	private int numberOfAttacks = 1;
	private bool isAttacking = false;
	private float attackTime = 0;
	private bool lookAtPlayer = false;
	private TrailRenderer trailRenderer;
	private bool trailChangin = false;

	public GameObject shotSpotLeft;
	public GameObject shotSpotRight;
	public GameObject bone;

	public GameObject bullet1;
	public GameObject bullet2;
	private List<GameObject> bullet2List;
	public float rotationStepSize = 0.08f;
	public float fireRateAttack1 = 10;
	public int attack2noAttacks = 25;

	private LineRenderer line;

	public GameObject loopCurve;
	public GameObject circleCurve;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		player = GameObject.FindWithTag ("Player");
		trailRenderer = GetComponent<TrailRenderer> ();
		line = GetComponent<LineRenderer> ();
		line.enabled = false;
		line.positionCount = 2;
		trailRenderer.time = 1;
		rb = GetComponent<Rigidbody> ();

		bone.transform.RotateAround (transform.position, transform.up, -90);
		shotSpotLeft.transform.RotateAround (transform.position, transform.up, -90);
		shotSpotRight.transform.RotateAround (transform.position, transform.up, -90);
		pointToLookAt = player.transform.position;
	}

	void FixedUpdate(){

		if (begining) {
			rb.velocity = new Vector3(0,0, -2.0f*(transform.position.z-6.0f));
			begining = transform.position.z < 7 ? false : true;
			if (!begining) {
				currentAttackPattern = 1;
				rb.velocity = Vector3.zero;
				defPosition = transform.position;
			}
				
		} else {
			// TODO kun enemmän hyökkäyksiä, ei tee samaa toistamiseen
			if (!isAttacking && !begining) {
				isAttacking = true;
				if (currentAttackPattern == 1) {
					StartCoroutine (Attack1 ());
				}else {
					StartCoroutine (Attack2 ());
				}
			}
			pointToLookAt = lookAtPlayer ? player.transform.position : pointToLookAt;
			lookAt (pointToLookAt, rotationStepSize);
		
		}
			


		attackTime -= Time.fixedDeltaTime;
		animator.SetFloat ("animSpeed", animSpeed);
		animator.SetInteger ("animIndex", animIndex);

	} // FixedUpdate


	IEnumerator returnToMiddle(){
		Vector3 sub = (defPosition - transform.position);

		while (sub.magnitude > 0.1f) {
			rb.velocity = sub + sub.normalized;
			sub = (defPosition - transform.position);
			yield return new WaitForFixedUpdate();
		}
		rb.velocity = Vector3.zero;
		lookAtPlayer = true;
		isAttacking = false;
	}
		
	void lookAt(Vector3 target, float stepSize){
		Vector3 newDir = Vector3.RotateTowards (transform.forward, transform.position- target, stepSize, 0);
		transform.rotation = Quaternion.LookRotation(newDir);
	}

	IEnumerator goToPoint(Vector3 point, float speed, bool deaccelerate){
		Vector3 sub = (point - transform.position);

		while (sub.magnitude > 3f) {
			rb.velocity = deaccelerate ? sub + sub.normalized*speed : sub.normalized*speed;
			sub = (point - transform.position);
			yield return new WaitForFixedUpdate();
		}
		rb.velocity = Vector3.zero;
	}

	IEnumerator Attack1(){
		if (shipMode) {
			ChangeState ();
		}
		lookAtPlayer = true;
		yield return new WaitForSeconds (2);
		StartCoroutine(trailRendererTimeChanger (0f, 0.3f));
		attackTime = Random.Range (10.0f, 16.0f);
		GameObject curve = Instantiate (loopCurve, transform);
		curve.transform.parent = null;
		curve.transform.rotation = new Quaternion ();
		StartCoroutine(followCurve(curve,0.005f));
		yield return new WaitForSeconds (0.6f);
		while (attackTime > 0) {
			yield return new WaitForSeconds (Time.fixedDeltaTime*fireRateAttack1);
			Instantiate (bullet1, shotSpotLeft.transform.position,new Quaternion());
			Instantiate (bullet1, shotSpotRight.transform.position,new Quaternion());
		}
		currentAttackPattern = 2;
		StartCoroutine (returnToMiddle());

	}

	IEnumerator Attack2(){
		if (!shipMode) {
			ChangeState ();
		}
		StartCoroutine(trailRendererTimeChanger (0.2f, 0.2f));
		GameObject circleGO = Instantiate(circleCurve, new Vector3(0,0,5), new Quaternion());
		circleGO.transform.parent = null;
		BezierCurve circle = circleGO.GetComponent<BezierCurve> ();

		BoxCollider collider = GetComponent<BoxCollider> ();

		collider.size = new Vector3(collider.size.x ,collider.size.y ,8);


		// Alku
		Vector3 firstGo = new Vector3 (Random.Range (-20f, 20f), 0f, -20f);
		lookAtPlayer = false;
		pointToLookAt = firstGo;
		float rotationStepSizeOLD = rotationStepSize;
		rotationStepSize = 0.01f;
		yield return new WaitForSeconds (2);
		line.SetPosition (0, transform.position);
		line.SetPosition (1, firstGo);
		yield return new WaitForSeconds (0.3f);
		StartCoroutine (LineFlash ());
		yield return new WaitForSeconds (0.8f);
		StartCoroutine (goToPoint (firstGo, 150f, false));
		yield return new WaitForSeconds (0.2f);

		while (rb.velocity.magnitude > 0.1f){
			yield return new WaitForFixedUpdate();
		}
		rotationStepSize = 1f;
		float firstPoint, secondPoint;
		Vector3 fpv, spv;
		bullet2List = new List<GameObject> ();

		yield return new WaitForSeconds (0.2f);
		trailRenderer.enabled = false;

		for (int i = 0; i < attack2noAttacks; i++) {

			// 1. Hae pisteet
			firstPoint = Random.Range (0.0f, 1.0f);
			secondPoint = firstPoint + Random.Range (0.45f, 0.55f);
			secondPoint = secondPoint > 1f ? secondPoint - 1.0f : secondPoint;

			if (i == attack2noAttacks - 1) {
				firstPoint = Random.Range (0.7f, 0.8f);;
				secondPoint = Random.Range (0.2f, 0.3f);
			}
			fpv = circle.GetPointAt (firstPoint);
			spv = circle.GetPointAt (secondPoint);

			// 2. Varoitus line
			line.SetPosition (0, fpv);
			line.SetPosition (1, spv);
			StartCoroutine (LineFlash ());

			yield return new WaitForSeconds (0.4f);

			// 3. Syöksy ja jätä ammukset
			transform.position = fpv;
			trailRenderer.enabled = false;
			pointToLookAt = spv;
			yield return new WaitForSeconds (0.2f);
			trailRenderer.enabled = true;
			StartCoroutine(goToPoint(spv,150f,false));

			 yield return new WaitForSeconds (Random.Range(0.075f, 0.115f));
			bullet2List.Add(Instantiate (bullet2, transform.position,new Quaternion()));
			yield return new WaitForSeconds (Random.Range(0.01f, 0.1f));
			bullet2List.Add(Instantiate (bullet2, transform.position,new Quaternion()));
			yield return new WaitForSeconds (Random.Range(0.01f, 0.035f));
			bullet2List.Add(Instantiate (bullet2, transform.position,new Quaternion()));

			while (rb.velocity.magnitude > 0.5f) {
				yield return new WaitForFixedUpdate ();
			}
		}

		yield return new WaitForSeconds (0.5f);

		collider.size = new Vector3(collider.size.x ,collider.size.y ,1.8f);

		foreach (GameObject b in bullet2List) {
			if (b != null) {
				b.GetComponent<TransformerBullet2Mover> ().Move ();
			}
		}

		yield return new WaitForSeconds (0.5f);

		rotationStepSize = rotationStepSizeOLD;
		Destroy (circleGO);
		pointToLookAt = defPosition;
		StartCoroutine(trailRendererTimeChanger (0f, 0.05f));
		currentAttackPattern = 1;
		StartCoroutine (returnToMiddle());
	}


	IEnumerator followCurve(GameObject curve, float speed){
		float curvePosition = 0;
		BezierCurve bCurve = curve.GetComponent<BezierCurve> ();
		while (attackTime > 0) { //TODO Slow start?
			transform.position = bCurve.GetPointAt (curvePosition);
			curvePosition += speed;
			curvePosition = curvePosition > 1 ? 1-curvePosition : curvePosition;
			yield return new WaitForFixedUpdate();
		}
		Destroy (curve);
	}

	// Used to enable/disable trailRenderer smoothly
	IEnumerator trailRendererTimeChanger(float time, float step){

		// Just make sure that only one of these runs at onnce
		while (trailChangin) {
			yield return new WaitForFixedUpdate ();
		}

		if (trailRenderer.time < time) {
			while (trailRenderer.time < time) {
				trailChangin = true;
				trailRenderer.time += step;
				yield return new WaitForFixedUpdate ();

			}
		} else {
			while (trailRenderer.time > time || trailRenderer.time > 0) {
				trailChangin = true;
				trailRenderer.time = trailRenderer.time - step > 0 ? trailRenderer.time - step : 0;
				yield return new WaitForFixedUpdate ();
			}
		}
		trailChangin = false;
	}

	IEnumerator LineFlash(){
		line.startWidth = 0;
		line.enabled = true;
		int dur = 30;
		for (int i = 1; i < dur; i++) {
			line.startWidth = i < dur / 2 ? (float)i / (float)dur / 2.0f : (float)(dur - i) / (float)dur / 2.0f;
			line.startWidth *= 2;
			yield return new WaitForFixedUpdate ();
		}
		line.enabled = false;

	}

	private void ChangeState(){
		animIndex = Random.Range (1, 6);
		animIndex *= shipMode ? 1 : -1;
		animSpeed = shipMode ? -1 : 1;
		shipMode = !shipMode;
	}

	void OnDestroy(){
		if (bullet2List != null) {
			foreach (GameObject b in bullet2List) {
				if (b != null) {
					b.GetComponent<TransformerBullet2Mover> ().Move ();
				}
			}
		}
	}
}
