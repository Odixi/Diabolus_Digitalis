﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurveMover : MonoBehaviour
{
    public BezierCurve movementCurve;
    public List<float> curvePhaseChangeDistance;
    public List<float> speed;
    public List<bool> rotateToCurve;
    public List<bool> rotateToPlayer;
    public float curveForwardSpeed;
    public float stopDuration;
    public float rotationSpeed;
    public bool jumpToCurveStart;

    protected Rigidbody rb;
    private Quaternion targetRotation;
    private GameObject playerObject;
    protected Vector3 nextCurvePoint;
    protected Vector3 tempMovement;
    protected bool dontMove;
    protected float curvePosition;
    private float currentTime;
    protected int curvePhase;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        playerObject = GameObject.FindGameObjectWithTag("Player");
        curvePosition = 0.0f;
        curvePhase = 0;
        dontMove = false;

        // If a list has too few values listed, the required amount is automatically added to avoid going out of bounds.
        if (speed.Count <= curvePhaseChangeDistance.Count)
        {
            for (int j = 0; j <= curvePhaseChangeDistance.Count - speed.Count; j++)
            {
                speed.Add(speed[0]);
            }
        }
        if (rotateToCurve.Count <= curvePhaseChangeDistance.Count)
        {
            for (int j = 0; j <= curvePhaseChangeDistance.Count - rotateToCurve.Count; j++)
            {
                rotateToCurve.Add(false);
            }
        }
        if (rotateToPlayer.Count <= curvePhaseChangeDistance.Count)
        {
            for (int j = 0; j <= curvePhaseChangeDistance.Count - rotateToPlayer.Count; j++)
            {
                rotateToPlayer.Add(false);
            }
        }
    }

    /*
     * The object's speed and rotation can be changed once curvePhaseChangeDistance[curvePhase] has been travelled. 
     * Once curvePhaseChangeDistance[curvePhase] is reached, the speed of the ship is changed to speed[curvePhase+1].
     * If shipSpeed[curvePhase] is 0, the object will begin moving again after stopDuration has elapsed, after which curvePhase will be incremented.
     */
    void FixedUpdate()
    {
        // Change speed if the curvePhaseChangeDistance[curvePhase] has been travelled
        if (curvePhase < curvePhaseChangeDistance.Count && curvePosition >= curvePhaseChangeDistance[curvePhase])
        {
            curvePhase++;
            // If speed is 0, start a timer so movement can eventually continue
            if (speed[curvePhase] == 0.0f)
            {
                dontMove = true;
                currentTime = Time.time;
            }
        }
        /*
         * If stopDuration has elapsed after the object has stopped, continue movement with the next speed.
         * The speed.Count check is used to avoid going out of bounds.
         */
        if (dontMove && curvePhase < (speed.Count - 1) && (Time.time - currentTime) >= stopDuration)
        {
            dontMove = false;
            curvePhase++;
            //rb.transform.rotation = Quaternion.Euler(GetComponent<EntityUserData>().spawnRotations[curvePhase]);
        }
        if (rotateToPlayer[curvePhase])
        {
            targetRotation = Quaternion.LookRotation(playerObject.transform.position - transform.position);
            rb.transform.rotation = Quaternion.Lerp(rb.transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
        }
        if (!dontMove)
        {
            if (curvePosition >= 0.99f)
            {
                if (jumpToCurveStart == true)
                {
                    curvePosition = 0.0f;
                    curvePhase = 0;
                }
                else
                {
                    dontMove = true;
                }
            }
            curvePosition = curvePosition + speed[curvePhase];
            nextCurvePoint = movementCurve.GetPointAt(curvePosition); // According to the profiler, this is relatively expensive
            if (rotateToCurve[curvePhase])
            {
                rb.transform.LookAt(nextCurvePoint);
            }
            rb.transform.position = nextCurvePoint;
            tempMovement = movementCurve.transform.position;
            tempMovement.z -= curveForwardSpeed;
            movementCurve.transform.position = tempMovement;
        }
    }
}
