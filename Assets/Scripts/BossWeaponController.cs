﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossWeaponController : MonoBehaviour
{
    public float primaryWaveInitialDelay;
    public float primaryWaveWait;
    public int primaryShotsPerWave;
    public float primaryFireRate;

    public float secondaryWaveInitialDelay;
    public float secondaryWaveWait;
    public int secondaryShotsPerWave;
    public float secondaryFireRate;

    public GameObject primaryShot;
    public GameObject secondaryShot;
    public List <GameObject> primaryShotSpawns;
    public List <GameObject> secondaryShotSpawns;

    private bool rotateLeft = true;
    private Transform shotTransform;

    void Start()
    {
        StartCoroutine(ShootPrimaryWaves());
        StartCoroutine(ShootSecondaryWaves());
    }

    IEnumerator ShootPrimaryWaves()
    {
        yield return new WaitForSeconds(primaryWaveInitialDelay);
        while (true)
        {
            yield return StartCoroutine(ShootWave());
        }
    }

    IEnumerator ShootSecondaryWaves()
    {
        yield return new WaitForSeconds(secondaryWaveInitialDelay);
        while (true)
        {
            yield return StartCoroutine(ShootCenter());
        }
    }

    IEnumerator ShootCenter()
    {
        for (int i = 0; i < secondaryShotsPerWave; i++)
        {
            for (int j = 0; j < secondaryShotSpawns.Count; j++) {
                shotTransform = secondaryShotSpawns[j].GetComponent<Transform>();
                Instantiate(secondaryShot, shotTransform.position, shotTransform.rotation);
            }
            yield return new WaitForSeconds(secondaryFireRate);
        }
        yield return new WaitForSeconds(secondaryWaveWait);
    }

    IEnumerator ShootWave()
    {
        for (int i = 0; i < primaryShotsPerWave; i++)
        {
            for (int j = 0; j < primaryShotSpawns.Count; j++) {
                shotTransform = primaryShotSpawns[j].GetComponent<Transform>();
                Instantiate(primaryShot, shotTransform.position, shotTransform.rotation);
            }
            yield return new WaitForSeconds(primaryFireRate);
        }
        yield return new WaitForSeconds(primaryWaveWait);
    }

    /*
    void ShootRotating()
    {
		if (centerShotSpawn0.rotation.eulerAngles.y > 210) {
			rotateLeft = false;
		} else if (centerShotSpawn0.rotation.eulerAngles.y < 150) {
			rotateLeft = true;
		}
		FireRotating(centerShotSpawn0, rotateLeft);
		FireRotating(centerShotSpawn1, rotateLeft);
		FireRotating(centerShotSpawn2, rotateLeft);
		FireRotating(centerShotSpawn3, rotateLeft);
		FireRotating(centerShotSpawn4, rotateLeft);
		FireRotating(centerShotSpawn5, rotateLeft);
    }

	void FireRotating(Transform shotSpawn, bool rotateleft) 
	{
		if (rotateleft) {
			shotSpawn.Rotate (0.0f, rotationspeed, 0.0f);
		} else {
			shotSpawn.Rotate (0.0f, -rotationspeed, 0.0f);
		}
		Instantiate (shotRotating, shotSpawn.position, shotSpawn.rotation);
	}
	*/

}