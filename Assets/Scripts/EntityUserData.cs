﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityUserData : MonoBehaviour {

    public List<Vector3> spawnValues; // Determines where entity can spawn, having the variable here makes enemy spawning easier
    public List<Vector3> spawnRotations;
    public int health;
	public int damage;
    public int points;
	public int resourceMin;
	public int resourceMax;
    public bool dropsResource;
    public bool shouldExplode; //Bullets doun't exploade themselves
    public bool randomizeSpawnValues;

	public virtual void takeDamage(int damage)
    {
		health -= damage;
	}

	public int resourceAmount()
    {
        if (dropsResource)
        {
            return Random.Range(resourceMin, resourceMax + 1);
        }
        return 0;
	}
}
