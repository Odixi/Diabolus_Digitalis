﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TextRaiser : MonoBehaviour {
	public float speed;
	public Rigidbody rb;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		rb.velocity = Vector3.up * speed;
		if (transform.position.y > 800) {
			SceneManager.LoadScene("Main_Menu");
		}
		if(Input.GetKeyDown(KeyCode.Escape)){
			SceneManager.LoadScene("Main_Menu");
		}
	}
}
