﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoverMainMenu : MonoBehaviour {

	private float counter;
	private float counterRot;
	private Vector3 tempMove;
	private Vector3 tempRotate;

	public Vector3 rotation;
	public float speedMovement;
	public float speedRotation;
	public float sizeOfMovement;
	public float startingAngle;



	// Use this for initialization
	void Start () {
		counter = startingAngle;
	}


	void FixedUpdate(){

		tempMove.x = gameObject.transform.localPosition.x + Mathf.Sin (counter) * sizeOfMovement;
		tempMove.y = gameObject.transform.localPosition.y + Mathf.Cos (counter) * sizeOfMovement;

		gameObject.transform.localPosition = tempMove;

		tempRotate.x = rotation.x * Mathf.Sin (counter);
		tempRotate.y = rotation.y * Mathf.Sin (counter);
		tempRotate.z = rotation.z * Mathf.Sin (counter);

		gameObject.transform.Rotate (tempRotate);

		counter += speedMovement;
		if (counter >= 360){
			counter = 0;
		}
		counterRot += speedRotation;
		if (counterRot >= 360){
			counterRot = 0;
		}
	}
}
