﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUserData : EntityUserData {

	public int shield;
	public GameObject shieldExplosion;

	public override void takeDamage(int damage)
	{
		if (shield > 0){
			shield -= damage;
			if (shield <= 0) {
				health += shield;
				shield = 0;
				Instantiate (shieldExplosion, transform.position, transform.rotation);
			}
		}
		else {
			health -= damage;
		}

		this.GetComponent<PlayerController> ().whenHit();

	}
}
