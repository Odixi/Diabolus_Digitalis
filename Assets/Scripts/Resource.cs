﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resource : MonoBehaviour
{
	public float speed;

    private Vector3 movement;
    private Rigidbody rb;
    private int resourceAmount;

	void Start()
    {
		rb = GetComponent<Rigidbody> ();
		movement = new Vector3 (Random.value*speed, 0, Random.value*speed - 800);
		rb.AddForce (movement);
		movement = new Vector3 (Random.value*speed, Random.value*speed, Random.value*speed);
		rb.AddTorque (movement);
		float multipler = Mathf.Pow (GameObject.Find ("GameController").GetComponent<GameController> ().enemyResourceDropRateMultipler, GameSessionData.wave - 1);
		resourceAmount = Random.Range ((int)(7*multipler), (int)(13*multipler));
	}

	public int getResourceAmount()
    {
		return resourceAmount;
	}
}
