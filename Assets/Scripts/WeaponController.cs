﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public GameObject shot;
    public List<GameObject> shotSpawns;
    public bool shootStraight;
    public bool randomizeDelay;
    public float fireRate;
    public float initialFiringDelay;
    public float minRandomInitialFiringDelay;
    public float maxRandomInitialFiringDelay;

    private Quaternion straightRotation;

    void Start()
    {
        if (randomizeDelay == true)
        {
            initialFiringDelay = UnityEngine.Random.Range(minRandomInitialFiringDelay, maxRandomInitialFiringDelay);
        }

        // If the enemy should shoot straight along the Z axis instead of the direction the ship is facing, use ShootStraight
        if (shootStraight == true)
        {
            straightRotation.Set(0.0f, -180.0f, 0.0f, 0.0f);
            InvokeRepeating("ShootStraight", initialFiringDelay, fireRate);
        }
        else
        {
            InvokeRepeating("Shoot", initialFiringDelay, fireRate);
        }
    }

    void Shoot()
    {
		GameObject shotInst;
        for (int i = 0; i < shotSpawns.Count; i++)
        {
            shotInst = Instantiate(shot, shotSpawns[i].GetComponent<Transform>().position, shotSpawns[i].GetComponent<Transform>().rotation);
			// Increase damage based on wave number
			if (GameSessionData.mode == GameMode.ARCADE) {
				try{
					shotInst.GetComponent<EntityUserData> ().damage = (int)(shotInst.GetComponent<EntityUserData> ().damage*Mathf.Pow (GameObject.Find ("GameController").GetComponent<EnemySpawner> ().enemyDamageIncreaseMultipler, GameSessionData.wave));
				}catch(Exception e){
					print (e);
				}
			}
		}
    }

    void ShootStraight()
    {	
		GameObject shotInst;
        for (int i = 0; i < shotSpawns.Count; i++)
        {
			shotInst = Instantiate(shot, shotSpawns[i].GetComponent<Transform>().position, straightRotation);
			if (GameSessionData.mode == GameMode.ARCADE) {
				try{
					shotInst.GetComponent<EntityUserData> ().damage = (int)(shotInst.GetComponent<EntityUserData> ().damage*Mathf.Pow (GameObject.Find ("GameController").GetComponent<EnemySpawner> ().enemyDamageIncreaseMultipler, GameSessionData.wave));
				}catch(Exception e){
					print (e);
				}

			}
        }
    }
}