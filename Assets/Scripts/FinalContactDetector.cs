﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalContactDetector : MonoBehaviour {

	public GameObject finalSceneController;

	void OnTriggerEnter(Collider other){
		if (other.tag == "Player" && gameObject.name == "Portal") {
			finalSceneController.GetComponent<FInalSceneController> ().PortalEntered (other.gameObject);
		} else if (other.tag == "PlayerShot" && gameObject.name == "Portal") {
			Destroy (other.gameObject);
			finalSceneController.GetComponent<FInalSceneController> ().PortalDestroyed(gameObject);
		} else if (other.tag == "Player" && gameObject.name == "AlternativeEndingTrigger") {
			finalSceneController.GetComponent<FInalSceneController> ().PortalLeftUntouched (other.gameObject);
		}
	}
}
