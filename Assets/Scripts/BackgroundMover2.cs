﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMover2 : MonoBehaviour {

	public float speed;
	public float bgLength;
	private Vector3 tempMove;
	private Rigidbody rb;

	// Use this for initialization
	void Start () {
		rb = gameObject.GetComponent<Rigidbody> ();
	}

	void FixedUpdate()
	{
		if (gameObject.transform.localPosition.z <= -bgLength) {
			tempMove = gameObject.transform.localPosition;
			tempMove.z = bgLength*2 + gameObject.transform.localPosition.z;
			gameObject.transform.localPosition = tempMove;
		}

		rb.velocity = new Vector3 (0, 0,speed);
	}
		
}
