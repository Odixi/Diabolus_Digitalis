﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class DestroyByContact : MonoBehaviour
{
    private GameController gameController;
    public GameObject explosion;
    public GameObject playerExplosion;
    public GameObject smallExplosion;
    public GameObject resourcePrefab;

    private AudioClip playerHitSound;
    private AudioClip enemyHitSound;
    private AudioClip explosionSound;

    private bool canDropResource = false;

    private GameObject playerObject;
    private Component playerData;
    private void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        gameController = gameControllerObject.GetComponent<GameController>();
        playerObject = GameObject.FindGameObjectWithTag("Player");

		playerHitSound = Resources.Load<AudioClip> ("PlayerTakesDamageSound");
		enemyHitSound = Resources.Load<AudioClip> ("EnemyTakesDamageSound");
		explosionSound = Resources.Load<AudioClip> ("ExplosionSound");
    }

    private void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
		// Resurssien saaminen TODO efekti
		if (other.tag == "Resource" || this.tag == "Resource") {
            if (other.tag == "Player") 
            {
                if (GameSessionData.isStoryMode() && (playerObject.GetComponent<PlayerUserData>().health < 120))
                {
                    playerObject.GetComponent<PlayerUserData>().health += 1;
                    Destroy(gameObject);
                }
                else
                {
                    PlayerController.resource += gameObject.GetComponent<Resource>().getResourceAmount();
                    Destroy(gameObject);
                }
            }
			return; // resurssit ei välitä muista
		}
			
		if (other.tag == "Boundary" || other.tag == "Enemy" || other.tag == "BossTransformer" 
			|| other.tag == "EnemyNoCurve" || other.tag == "EnemyShot" || other.tag == "Boss" || other.tag == "SuperLaser")
        {
            return;
        }
        if (other.tag == "PlayerShot" && this.tag == "EnemyShot")
        {
            return;
        }

		if (other.tag == "Player")
		{
			if (this.tag != "BossTransformer") { // Transformer doesn't take damage from player
				gameObject.GetComponent<EntityUserData> ().takeDamage (other.gameObject.GetComponent<EntityUserData> ().damage);
			} else {
				Instantiate (smallExplosion, transform.position, transform.rotation);
				GameController.fxSoundSource.PlayOneShot (playerHitSound);
			}
			other.gameObject.GetComponent<EntityUserData> ().takeDamage (gameObject.GetComponent<EntityUserData> ().damage);


			if (gameObject.GetComponent<EntityUserData> ().health <= 0) {
				if (gameObject.GetComponent<EntityUserData> ().shouldExplode) {
					Instantiate (explosion, transform.position, transform.rotation);
					GameController.fxSoundSource.PlayOneShot (explosionSound);
				} else {
					Instantiate (smallExplosion, transform.position, transform.rotation);
					GameController.fxSoundSource.PlayOneShot (playerHitSound);
				}
				PlayerController.score += gameObject.GetComponent<EntityUserData> ().points;
				if (tag == "Enemy" || tag == "BossTransformer")
				{
					Destroy(gameObject.GetComponent<CurveMover>().movementCurve.gameObject);
				}
				Destroy (gameObject);
			}
			if (other.gameObject.GetComponent<EntityUserData> ().health <= 0 || this.tag == "Boss") { 
				if (other.gameObject.GetComponent<EntityUserData> ().shouldExplode) { // Taitaa olla turha, koska player anyways...
					Instantiate (playerExplosion, transform.position, transform.rotation);
					GameController.fxSoundSource.PlayOneShot (explosionSound);
				} else {
					Instantiate (smallExplosion, transform.position, transform.rotation);
					GameController.fxSoundSource.PlayOneShot (playerHitSound);
				}
				PlayerController.score += other.gameObject.GetComponent<EntityUserData> ().points;
				//Destroy (other.gameObject);
				other.gameObject.GetComponent<PlayerController>().Die();
				gameController.GameOver();
			} 
			return;
		}
		if (explosion != null) {
            gameObject.GetComponent<EntityUserData> ().takeDamage (other.gameObject.GetComponent<EntityUserData> ().damage); //Entities take damage
			other.gameObject.GetComponent<EntityUserData> ().takeDamage (gameObject.GetComponent<EntityUserData> ().damage);
            //If health drops below 0, they die
            if (gameObject.GetComponent<EntityUserData>().health <= 0) {
                if (gameObject.GetComponent<EntityUserData>().shouldExplode) {
                    Instantiate(explosion, transform.position, transform.rotation);
					GameController.fxSoundSource.PlayOneShot (explosionSound);
                    // Pudottaa resursseja
                    for (int i = 0; i < gameObject.GetComponent<EntityUserData>().resourceAmount(); i++)
                    {
                        Instantiate(resourcePrefab, transform.position, transform.rotation);
                    }

                } else {
                    Instantiate(smallExplosion, transform.position, transform.rotation);
					GameController.fxSoundSource.PlayOneShot (enemyHitSound);
                }
                PlayerController.score += gameObject.GetComponent<EntityUserData>().points;
                // Destroy enemy movement curve if it has one
				if (tag == "Enemy")
                {
                    Destroy(gameObject.GetComponent<CurveMover>().movementCurve.gameObject);
                }
                Destroy (gameObject);
			}
			if (other.gameObject.GetComponent<EntityUserData> ().health <= 0) {
				if (other.gameObject.GetComponent<EntityUserData> ().shouldExplode) {
					Instantiate (explosion, transform.position, transform.rotation);
					GameController.fxSoundSource.PlayOneShot (explosionSound);
                    // Pudotetaan resursseja
                    for (int i = 0; i < gameObject.GetComponent<EntityUserData>().resourceAmount(); i++)
                    {
                        Instantiate(resourcePrefab, transform.position, transform.rotation);
                    }

				} else {
					Instantiate (smallExplosion, transform.position, transform.rotation);
					GameController.fxSoundSource.PlayOneShot (enemyHitSound);
				}
				PlayerController.score += other.gameObject.GetComponent<EntityUserData> ().points;
				Destroy (other.gameObject);
			}
        }
    }

    public void setCanDropResource(bool canDrop)
    {
       canDropResource = canDrop;
    }
}


