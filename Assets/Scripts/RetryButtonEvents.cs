﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RetryButtonEvents : MonoBehaviour
{
    public Button startGameButton;
    public Button mainMenuButton;
    public Button quitGameButton;

    void Start ()
    {
        var btnStartGame = startGameButton.GetComponent<Button>();
        var btnMainMenu = mainMenuButton.GetComponent<Button>();
        var btnQuitGame = quitGameButton.GetComponent<Button>();

        btnStartGame.onClick.AddListener(onStartGameClicked);
        btnMainMenu.onClick.AddListener(onMainMenuClicked);
        btnQuitGame.onClick.AddListener(onQuitGameClicked);
    }

    void onStartGameClicked()
    {
        SceneManager.LoadScene(2);
    }

    void onMainMenuClicked()
    {
        SceneManager.LoadScene(0);
    }

    void onQuitGameClicked()
    {
        Application.Quit();
    }
}
