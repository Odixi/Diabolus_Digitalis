﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalAnimation : MonoBehaviour {

	public float angularSpeed;
	public bool x,y,z;


	void LateUpdate () {
		if (z) {
			transform.RotateAround (transform.position, transform.up, angularSpeed);
		}
		if (y) {
			transform.RotateAround (transform.position, transform.forward, angularSpeed);
		}
		if (x) {
			transform.RotateAround (transform.position, transform.right, angularSpeed);
		}
	}
}
