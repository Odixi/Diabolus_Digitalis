﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoverEye : MonoBehaviour
{
	public float speed;
	public float rotationSpeed;

	private Rigidbody rb;
	private GameObject playerObject;

	void Start()
	{	
		transform.GetChild (0).transform.Rotate (0, 180, 0);

		if (playerObject = GameObject.FindGameObjectWithTag ("Player")) {
			rb = GetComponent<Rigidbody> ();
			rb.transform.LookAt (playerObject.transform);
			rb.velocity = transform.forward * speed;
		} else {
			Destroy (this.gameObject);
		}
	}

	void FixedUpdate()
	{
		if (playerObject = GameObject.FindGameObjectWithTag ("Player")) {

			Vector3 playerMinusThis = playerObject.transform.position - transform.position;
			Quaternion lookRot = Quaternion.LookRotation (playerMinusThis);

			transform.rotation = (Quaternion.RotateTowards(transform.rotation, lookRot, rotationSpeed));
			rb.velocity = transform.forward * speed;

		}

	}
}