﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalExplosion : MonoBehaviour {

	public float explosionDur;
	public AnimationCurve size;
	public float sizeMult;
	public float startAlpha;

	private Material mat;
	private float alpha;
	private float scale;

	// Use this for initialization
	void Start () {
		mat = GetComponent<Renderer> ().material;
		alpha = startAlpha;
		scale = 0;
	}
	
	// Update is called once per frame
	void Update () {
		alpha -= Time.deltaTime / explosionDur * startAlpha;
		alpha = alpha < 0 ? 0 : alpha;
		mat.SetColor("_TintColor",new Color(1,1,1,alpha));
		scale += Time.deltaTime / explosionDur;
		transform.localScale = new Vector3(1,1,1) * sizeMult * size.Evaluate (scale);
		print (alpha);
		print (transform.localScale);
	}
}
