﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LimitName : MonoBehaviour {

    public InputField nameInput;
    private int maxChars = 15;

	void Start () {
        nameInput.characterLimit = maxChars;
	}
}
