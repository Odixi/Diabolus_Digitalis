﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMove : MonoBehaviour
{
	public float speed;
	private Vector3 tempMove;

	void FixedUpdate()
    {
		if (gameObject.transform.localPosition.y <= -1.073f) {
			tempMove = gameObject.transform.localPosition;
			tempMove.y = 1.073f;
			gameObject.transform.localPosition = tempMove;
		}

		tempMove = gameObject.transform.localPosition;
		tempMove.y -= speed;
		gameObject.transform.localPosition = tempMove;
	}
}
