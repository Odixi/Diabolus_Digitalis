﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StoreControllerV2 : MonoBehaviour {

	public GameObject scoreText;
	public GameObject cashText;
	public GameObject nextWaveText;

	public GameObject damageUpgradePanel;
	public GameObject shieldUpgradePanel;
	public GameObject repairPanel;
	public GameObject superLaserPanel;

	public GameObject btnNextWave;

	private Button btnBuyDamage;
	private Button btnBuyShield;
	//private Button btnSmallRepair;
	private Button btnFullRepair;
	private Button btnBuyLaser;

	private Text lvlDamage;
	private Text lvlShield;
	private Text healthText;
	private Text lvlLaser;

	private Text upgtDamage;
	private Text upgtShield;
	private Text upgtFullRepair;
	private Text upgtLaser;

	private Text costDamageText;
	private Text costShieldText;
	//private Text costSmallRepairText;
	private Text costFullRepairText;
	private Text costLaserText;

	public float damageUpgradeMultipler;
	public float shieldUpgradeMultipler;
	public float laserUpgradeMultipler;
	public int laserUpgradeCost;

	private int costDamage;
	private int costShield;
	private int costSmallRepair;
	private int costFullRepair;
	private int costLaser;

	// Use this for initialization
	void Start () {

		Cursor.visible = true;
		
		btnBuyDamage = damageUpgradePanel.GetComponentInChildren<Button> ();
		btnBuyShield = shieldUpgradePanel.GetComponentInChildren<Button> ();
		Button[] repairBtns = repairPanel.GetComponentsInChildren<Button> ();
		foreach (Button b in repairBtns) {
			if (b.gameObject.name == "btnSmallRepair") {
			//	btnSmallRepair = b;
			} else if (b.gameObject.name == "btnFullRepair") {
				btnFullRepair = b;
			} 
		}// foreach repair buttons
		btnBuyLaser = superLaserPanel.GetComponentInChildren<Button>();

		Text[] dmgPanelTexts = damageUpgradePanel.GetComponentsInChildren<Text> ();
		foreach (Text t in dmgPanelTexts) {
			if (t.gameObject.name == "CostValue") {
				costDamageText = t;
			} else if (t.gameObject.name == "UpgradeLvl") {
				lvlDamage = t;
			} else if (t.gameObject.name == "UpgradeText") {
				upgtDamage = t;
			}
		}

		Text[] healthPanelTexts = shieldUpgradePanel.GetComponentsInChildren<Text> ();
		foreach (Text t in healthPanelTexts) {
			if (t.gameObject.name == "CostValue") {
				costShieldText = t;
			} else if (t.gameObject.name == "UpgradeLvl") {
				lvlShield = t;
			} else if (t.gameObject.name == "UpgradeText") {
				upgtShield = t;
			}
		}

		Text[] repairPanelTexts = repairPanel.GetComponentsInChildren<Text> ();
		foreach (Text t in repairPanelTexts) {
			if (t.gameObject.name == "CostValueFull") {
				costFullRepairText = t;
			} else if (t.gameObject.name == "CostValueSmall") {
				//costSmallRepairText = t;
			} else if (t.gameObject.name == "HealthLabel") {
				healthText = t;
			} else if (t.gameObject.name == "UpgradeFullText") {
				upgtFullRepair = t;
			}
		}

		Text[] laserPanelTexts = superLaserPanel.GetComponentsInChildren<Text> ();
		foreach (Text t in laserPanelTexts) {
			if (t.gameObject.name == "CostValue") {
				costLaserText = t;
			} else if (t.gameObject.name == "UpgradeLvl") {
				lvlLaser = t;
			} else if (t.gameObject.name == "Description") {
				upgtLaser = t;
			}
		}
			
			
		btnBuyDamage.onClick.AddListener (onBuyDamageButtonClicked);
		btnBuyShield.onClick.AddListener (onBuyShieldUpgradeClicked);
		//btnSmallRepair.onClick.AddListener (onSmallRepairClicked);
		btnFullRepair.onClick.AddListener (onFullRepairClicked);
		btnBuyLaser.onClick.AddListener (onBuyLaserClicked);
		btnNextWave.GetComponent<Button> ().onClick.AddListener (onNextWaveClicked);


		refresh ();


	}

	private void refresh (){
		
		costDamage = (int)(GameSessionData.priceOfDamageUpgrade * Mathf.Pow (damageUpgradeMultipler, GameSessionData.upgradeDamage-1));
		costShield = (int)(GameSessionData.priceOfShieldUpgrade * Mathf.Pow (shieldUpgradeMultipler, GameSessionData.upgradeShield-1));
		costLaser = (int)(GameSessionData.priceOfLaserUpgrade * Mathf.Pow (laserUpgradeMultipler, GameSessionData.upgradeSuperLaser));
		costSmallRepair = (int)(40*GameSessionData.priceOfRepair);
		costFullRepair = (int)((GameSessionData.playerMaxHealth - GameSessionData.playerHealth )* GameSessionData.priceOfRepair) 
			>= GameSessionData.cash ? GameSessionData.cash : (int)((GameSessionData.playerMaxHealth - GameSessionData.playerHealth )* GameSessionData.priceOfRepair);

		lvlDamage.text = "lvl " + GameSessionData.upgradeDamage.ToString ();
		lvlShield.text = "lvl " + GameSessionData.upgradeShield.ToString ();
		lvlLaser.text = "lvl" + GameSessionData.upgradeSuperLaser.ToString ();
		healthText.text = "hp " + GameSessionData.playerHealth.ToString () + "/" + GameSessionData.playerMaxHealth.ToString ();

		upgtDamage.text = "Damage " + GameSessionData.playerActualDamage.ToString() + " -> " + (int)(GameSessionData.playerActualDamage * damageUpgradeMultipler);
		upgtShield.text = "Max shield " + GameSessionData.playerMaxShield.ToString() + " -> " + (int)(GameSessionData.playerMaxShield + 10);
		upgtLaser.text = GameSessionData.upgradeSuperLaser == 0 ? "An extremely powerful laser" : 
			"Decrease super laser cooldown and increase damage";

		upgtFullRepair.text = (int)((GameSessionData.playerMaxHealth - GameSessionData.playerHealth) * GameSessionData.priceOfRepair) >= GameSessionData.cash 
			? "Repair " + (int)(GameSessionData.cash / GameSessionData.priceOfRepair) + " damage"
			: "Repair all damage";

		costDamageText.text = costDamage.ToString();
		costShieldText.text = costShield.ToString ();
		//costSmallRepairText.text = costSmallRepair.ToString ();
		costFullRepairText.text = costFullRepair.ToString ();
		costLaserText.text = costLaser.ToString ();

		if (costDamage > GameSessionData.cash) {
			btnBuyDamage.interactable = false;
			costDamageText.color = Color.red;
		} else {
			btnBuyDamage.interactable = true;
			costDamageText.color = Color.green;
		}

		if (costShield > GameSessionData.cash) {
			btnBuyShield.interactable = false;
			costShieldText.color = Color.red;
		} else {
			btnBuyShield.interactable = true;
			costShieldText.color = Color.green;
		}

		if (costLaser > GameSessionData.cash) {
			btnBuyLaser.interactable = false;
			costLaserText.color = Color.red;
		} else {
			btnBuyLaser.interactable = true;
			costLaserText.color = Color.green;
		}

		if (costSmallRepair > GameSessionData.cash || GameSessionData.playerHealth == GameSessionData.playerMaxHealth) {
			//btnSmallRepair.interactable = false;
			//costSmallRepairText.color = Color.red;
		} else {
			//btnSmallRepair.interactable = true;
			//costSmallRepairText.color = Color.green;
		}

		if (costFullRepair > GameSessionData.cash || GameSessionData.cash == 0 || GameSessionData.playerHealth == GameSessionData.playerMaxHealth) {
			btnFullRepair.interactable = false;
			costFullRepairText.color = Color.red;
		} else {
			btnFullRepair.interactable = true;
			costFullRepairText.color = Color.green;
		}

		scoreText.GetComponent<Text> ().text = "Score " + GameSessionData.score;
		cashText.GetComponent<Text> ().text = "Cash " + GameSessionData.cash;
		nextWaveText.GetComponent<Text> ().text = "next wave\n" + (GameSessionData.wave);
	}



	private void onBuyDamageButtonClicked(){
		GameSessionData.cash -= costDamage;
		GameSessionData.upgradeDamage++;
		GameSessionData.playerActualDamage = (int)(GameSessionData.playerActualDamage * damageUpgradeMultipler);
		refresh ();

	}

	private void onBuyShieldUpgradeClicked(){
		GameSessionData.cash -= costShield;
		GameSessionData.upgradeShield++;
		GameSessionData.playerMaxShield = (int)(GameSessionData.playerMaxShield + 10);
		refresh ();
	}

	private void onBuyLaserClicked(){
		GameSessionData.cash -= costLaser;
		if (GameSessionData.upgradeSuperLaser == 0) {
			GameSessionData.priceOfLaserUpgrade = laserUpgradeCost;
		}
		GameSessionData.upgradeSuperLaser++;
		refresh ();
	}

	private void onSmallRepairClicked(){
		GameSessionData.cash -= costSmallRepair;
		GameSessionData.playerHealth = GameSessionData.playerHealth + 40 >= GameSessionData.playerMaxHealth ? GameSessionData.playerMaxHealth : GameSessionData.playerHealth + 40;
		refresh ();
	}

	private void onFullRepairClicked(){
		GameSessionData.playerHealth = GameSessionData.playerMaxHealth - GameSessionData.playerHealth >= (int)(GameSessionData.cash / GameSessionData.priceOfRepair )
			? GameSessionData.playerHealth + (int)(GameSessionData.cash / GameSessionData.priceOfRepair) : GameSessionData.playerMaxHealth;
		GameSessionData.cash -= costFullRepair;
		refresh ();
	}
		
	private void onNextWaveClicked(){
        SceneManager.LoadScene(GameSessionData.getGameModeToLoad());		
	}
		
}