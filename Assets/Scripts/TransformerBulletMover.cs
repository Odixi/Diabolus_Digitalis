﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformerBulletMover : MonoBehaviour {

	private GameObject target;
	private Rigidbody rb;
	public float acceleration = 1.001f;
	public float startSpeed = 10f;

	void Start(){
		target = GameObject.FindGameObjectWithTag ("Player");
		rb = GetComponent<Rigidbody> ();
		Vector3 direction = (transform.position - target.transform.position).normalized;
		direction.Scale(new Vector3(Random.Range(0.06f,1),0,Random.Range(0.06f,1)));
		rb.velocity = (direction * startSpeed * -1);
	}
		
	void FixedUpdate () {
		rb.velocity = rb.velocity * acceleration;
	}
}
