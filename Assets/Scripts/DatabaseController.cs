﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;
using System.Data;
using System;
using System.IO;

public class DatabaseController : MonoBehaviour
{
    private List<HighScore> highscores;
    private string filepath;

    private string dbName = "DB-HighScores.db";
    private string dbPath;
    private string assetPath;

    private string connection;

    void Awake()
    {
        highscores = new List<HighScore>();
        initDatabase();
        loadScores();
    }

    void initDatabase()
    {
        string dbPath = Path.Combine(Application.persistentDataPath, dbName);
        var dbTemplatePath = Path.Combine(Application.streamingAssetsPath, dbName);

        if (!File.Exists(dbPath))
        {
            File.Copy(dbTemplatePath, dbPath, true);
        }
        filepath = "URI=file:" + dbPath;
    }

    void loadScores()
    {
        highscores.Clear();

        IDbConnection dbconn;
        dbconn = (IDbConnection)new SqliteConnection(filepath);
        dbconn.Open();

        IDbCommand dbcmd = dbconn.CreateCommand();

        string sqlQuery = "SELECT Name, Score " + "FROM Highscores ORDER BY Score DESC";
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();

        while (reader.Read())
        {
            highscores.Add(new HighScore(reader.GetString(0), reader.GetInt32(1)));
        }

        reader.Close();
        dbcmd.Dispose();
        dbconn.Close();

    }

    public List<HighScore> getHighScores()
    {
        return highscores;
    }

    public bool canSave(HighScore highscore)
    {

        if (highscores.Count < 10)
        {
            return true;
        }

        foreach(HighScore savedScore in highscores) 
        {
            if (highscore.score > savedScore.score)
            {
                return true;
            }
        }

        return false;
    }

    public void save(HighScore highScore)
    {
        IDbConnection dbconn;
        dbconn = (IDbConnection)new SqliteConnection(filepath);
        dbconn.Open();

        IDbCommand dbcmd = dbconn.CreateCommand();

        string insertSQL = "INSERT INTO HighScores (Name, Score) VALUES (@name, @score)";

        dbcmd.Parameters.Add(new SqliteParameter("@name", highScore.name));
        dbcmd.Parameters.Add(new SqliteParameter("@score", highScore.score));

        dbcmd.CommandText = insertSQL;
        dbcmd.ExecuteReader();

        dbcmd.Dispose();
        dbconn.Close();

        loadScores();

        if (highscores.Count > 10)
        {
            removeLast();
        }
    }

    public void removeAllHighscores()
    {
        IDbConnection dbconn;
        dbconn = (IDbConnection)new SqliteConnection(filepath);
        dbconn.Open();
        IDbCommand dbcmd = dbconn.CreateCommand();

        string deleteAllSQL = "DELETE FROM Highscores";
        string vacuumSQL = "VACUUM";

        dbcmd.CommandText = deleteAllSQL;

        IDataReader reader = dbcmd.ExecuteReader();
        reader.Close();

        dbcmd.CommandText = vacuumSQL;

        reader = dbcmd.ExecuteReader();

        reader.Close();

        dbcmd.Dispose();
        dbconn.Close();

        initDatabase();
        loadScores();
    }

    private void removeLast()
    {
        IDbConnection dbconn;
        dbconn = (IDbConnection)new SqliteConnection(filepath);
        dbconn.Open();

        IDbCommand dbcmd = dbconn.CreateCommand();

        string deleteLastSQL = "DELETE FROM HighScores WHERE ID = (SELECT * FROM (" +
            "SELECT ID FROM HighScores ORDER BY Score ASC LIMIT 1 ) as t)";

        dbcmd.CommandText = deleteLastSQL;

        dbcmd.ExecuteNonQuery();

        dbcmd.Dispose();
        dbconn.Close();

        loadScores();
    }

}