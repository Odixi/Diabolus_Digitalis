﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedRotation : MonoBehaviour {
	public float z;
	private float y;
	private float x;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		x = transform.rotation.eulerAngles.x;
		y = transform.rotation.eulerAngles.y;
		transform.eulerAngles = new Vector3(x,y,z);
	}


}
