﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

/**
 * 
 * Pidetään tietoa kaikesta pelin tilaan liittyvästä
 * 
 **/

public class GameSessionData : MonoBehaviour {

    public static GameMode mode;

	public static int score;
	public static int cash;
	public static int wave;
	public static int playerHealth;
    public static int playerMaxHealth;
	public static int playerShield;
	public static int playerMaxShield;
    public static int playerActualDamage;
    public static short upgradeDamage;
	public static bool upgradeDoubleCannon;
	public static short upgradeShield;
	public static short upgradeSuperLaser;

	public static float priceOfRepair; // yhden helan korjaamisen hinta
    public static int priceOfShieldUpgrade;
    public static int priceOfDamageUpgrade;
	public static int priceOfLaserUpgrade;

    public static bool gameOver;

    void Awake () {
		DontDestroyOnLoad (this.GetComponent<Transform>());
        Restart();
	}

	public void Restart(){


        score = 0;
		cash = 0;
		wave = 1;
		playerHealth = 100;
        playerMaxHealth = playerHealth;
		playerShield = 20;
		playerMaxShield = 20;
		playerActualDamage = 16;
        upgradeDamage = 1;
		upgradeDoubleCannon = false;
		upgradeShield = 1;
		upgradeSuperLaser = 0;
		gameOver = false;

        setBeginPrices();
	}

    public void setBeginPrices()
    {
		priceOfRepair = 1.75f;
		priceOfShieldUpgrade = 500;
		priceOfDamageUpgrade = 625;
		priceOfLaserUpgrade = 800;
    }

    public static bool isStoryMode()
    {
        return mode == GameMode.STORY;
    }

    public static bool isArcadeMode()
    {
        return mode == GameMode.ARCADE;
    }

    public static string getGameModeToLoad()
    {
        switch (mode)
        {
            case GameMode.STORY:
                return "StoryMode";

            case GameMode.ARCADE:
                return "ArcadeMode";

            case GameMode.TEST:
                return "TestingScene";

            default:  return "";
            
        }
    }

}
