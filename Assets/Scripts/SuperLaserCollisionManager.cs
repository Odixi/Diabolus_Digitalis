﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperLaserCollisionManager : MonoBehaviour {

	public int damagePerTic;
	public int damagePerTicForBosses;
	public float duration;
	private float timeTaken;

	public GameObject explosion;
	public GameObject resourcePrefab;

	public AnimationCurve lightCurve;
	public AnimationCurve xScaleCurve;
	private Light light;
	private AudioSource audioSource;

	private float lightDefIntesity;

	private AudioClip enemyHitSound;
	private AudioClip explosionSound;

	// Use this for initialization
	void Start () {

		enemyHitSound = Resources.Load<AudioClip> ("EnemyTakesDamageSound");
		explosionSound = Resources.Load<AudioClip> ("ExplosionSound");
		light = GetComponentInChildren<Light> ();
		lightDefIntesity = light.intensity;
		audioSource = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		transform.rotation = Quaternion.LookRotation(Vector3.forward);

		light.intensity = lightDefIntesity * lightCurve.Evaluate (timeTaken / duration);
		transform.localScale = new Vector3(xScaleCurve.Evaluate (timeTaken / duration),1.8f,1);
	//	transform.GetChild(1).localScale = new Vector3(xScaleCurve.Evaluate (timeTaken / duration),1.8f,1);
		transform.GetChild(2).localScale = new Vector3(xScaleCurve.Evaluate (timeTaken / duration),1.8f,1);
		audioSource.volume = xScaleCurve.Evaluate (timeTaken / duration);

		timeTaken += Time.fixedDeltaTime;
		if (timeTaken > duration) {
			Destroy (gameObject);
		}
	}

	void OnTriggerStay(Collider other){

		EntityUserData eud = other.gameObject.GetComponent<EntityUserData> ();
		if (eud != null && other.tag != "Resource" && other.tag != "Player") {
			if (other.tag == "Boss" || other.tag == "BossTransformer") {
				eud.takeDamage (damagePerTicForBosses);
			} else {
				eud.takeDamage (damagePerTic);
			}
			if (eud.health <= 0) {
				if (eud.shouldExplode) {
					Instantiate (explosion, other.transform.position, other.transform.rotation);
					GameController.fxSoundSource.PlayOneShot (explosionSound);
				}
				// TODO drop resource
				Destroy (other.gameObject);
			} else if (eud.shouldExplode) {
				
			}
		}
	}
}
