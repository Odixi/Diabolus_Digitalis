﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {
    public bool x;
    public bool y;
    public bool z;
    public float angle;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if(y){
            // Rotate the object around its local X axis at angle degrees per second
            transform.Rotate(Vector3.up, angle);
        }
        if (x)
        {
            // Rotate the object around its local X axis at angle degrees per second
            transform.Rotate(Vector3.right, angle);
        }
        if (z)
        {
            // Rotate the object around its local X axis at angle degrees per second
            transform.Rotate(Vector3.forward, angle);
        }
    }
}
