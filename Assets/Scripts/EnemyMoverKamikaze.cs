﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoverKamikaze : MonoBehaviour
{
    public float speed;
    
    private Rigidbody rb;
    private GameObject playerObject;

    void Start()
    {
		if (playerObject = GameObject.FindGameObjectWithTag ("Player")) {
			rb = GetComponent<Rigidbody> ();
			rb.transform.LookAt (playerObject.transform);
			rb.velocity = transform.forward * speed;
		} else {
			Destroy (this.gameObject);
		}
    }
}