﻿/*using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StoreController : MonoBehaviour {

    public GameObject playerCashText;
    public GameObject playerHealthText;
    public GameObject playerDamageText;

    private GameObject firstMainButtonRow;
    private GameObject secondMainButtonRow;
    private GameObject shipRepairButtonRow;
    private GameObject healthUpgradeButtonRow;
    private GameObject damageUpgradeButtonRow;
    private GameObject btnBack;

    private List<GameObject> secondaryButtonRows;

    private Button btnRepairShip;
    private Button btnHealthUpgrades;
    private Button btnDamage;
    private Button btnExitStore;
    private Button btnBasicFix;
    private Button btnFullFix;
    private Button btnSmallHealthUpgrade;
    private Button btnBigHealthUpgrade;
    private Button btnSmallDamageUpgrade;
    private Button btnBigDamageUpgrade;

    private int playerCash;
    private int playerHealth;
    private int playerMaxHealth;
    private int playerDamage;
    private int playerScore;

    void Start () {

		Cursor.visible = true;

        firstMainButtonRow = GameObject.Find("FirstMainButtonRow");
        secondMainButtonRow = GameObject.Find("SecondMainButtonRow");
        shipRepairButtonRow = GameObject.Find("ShipRepairButtonRow");
        healthUpgradeButtonRow = GameObject.Find("HealthUpgradeButtonRow");
        damageUpgradeButtonRow = GameObject.Find("DamageUpgradeButtonRow");

        secondaryButtonRows = new List<GameObject>();

        btnRepairShip = GameObject.Find("btnRepairShip").GetComponent<Button>();
        btnHealthUpgrades = GameObject.Find("btnHealthUpgrades").GetComponent<Button>();
        btnDamage = GameObject.Find("btnDamage").GetComponent<Button>();
        btnBack = GameObject.Find("btnBack");
        btnExitStore = GameObject.Find("btnExitStore").GetComponent<Button>();
        btnBasicFix = GameObject.Find("btnBasicFix").GetComponent<Button>();
        btnFullFix = GameObject.Find("btnFullFix").GetComponent<Button>();
        btnSmallHealthUpgrade = GameObject.Find("btnSmallHealthUpgrade").GetComponent<Button>();
        btnBigHealthUpgrade = GameObject.Find("btnBigHealthUpgrade").GetComponent<Button>();
        btnSmallDamageUpgrade = GameObject.Find("btnSmallDamageUpgrade").GetComponent<Button>();
        btnBigDamageUpgrade = GameObject.Find("btnBigDamageUpgrade").GetComponent<Button>();

        playerCash = GameSessionData.cash;
        playerHealth = GameSessionData.playerHealth;
        playerMaxHealth = GameSessionData.playerMaxHealth;
        playerDamage = GameSessionData.playerActualDamage;
        playerScore = GameSessionData.score;

        playerCashText.GetComponent<Text>().text = playerCash + "";
        playerHealthText.GetComponent<Text>().text = playerHealth + "/" + playerMaxHealth;
        playerDamageText.GetComponent<Text>().text = playerDamage + "";

        setMainButtonsListeners();

        btnBack.GetComponent<Button>().onClick.AddListener(onBackButtonClicked);

        secondaryButtonRows.Add(shipRepairButtonRow);
        secondaryButtonRows.Add(healthUpgradeButtonRow);
        secondaryButtonRows.Add(damageUpgradeButtonRow);

        firstMainButtonRow.SetActive(true);
        secondMainButtonRow.SetActive(true);

        foreach(var row in secondaryButtonRows)
        {
            row.SetActive(false);
        }

        btnBack.SetActive(false);
        
	}

    public void setMainButtonsListeners()
    {
        btnRepairShip.onClick.AddListener(onRepairShipClicked);
        btnHealthUpgrades.onClick.AddListener(onHealthUpgradesClicked);
        btnDamage.GetComponent<Button>().onClick.AddListener(onDamageClicked);
        btnExitStore.onClick.AddListener(onExitStoreClicked);
    }

    private void onExitStoreClicked()
    {
        SceneManager.LoadScene(1);
    }

    public void onBackButtonClicked()
    {
        firstMainButtonRow.SetActive(true);
        secondMainButtonRow.SetActive(true);

        foreach(GameObject row in secondaryButtonRows)
        {
            if (row.activeInHierarchy)
            {
                row.SetActive(false);
            }
        }

        btnBack.SetActive(false);
        
    }

    void onRepairShipClicked()
    {
        firstMainButtonRow.SetActive(false);
        secondMainButtonRow.SetActive(false);
        shipRepairButtonRow.SetActive(true);
        btnBack.SetActive(true);

        btnBasicFix.GetComponentInChildren<Text>().text = "";
        btnBasicFix.GetComponentInChildren<Text>().text = "Basic Fix \n(" + GameSessionData.basicFixPrice + ")";
        btnFullFix.GetComponentInChildren<Text>().text = "";
        btnFullFix.GetComponentInChildren<Text>().text = "Full Fix \n(" + GameSessionData.fullFixPrice + ")";

        validateRepairButtons();

        btnBasicFix.onClick.AddListener(onBasicFixClicked);
        btnFullFix.onClick.AddListener(onFullFixCLicked);
    }

    void onBasicFixClicked()
    {
        playerCash -= GameSessionData.basicFixPrice;
        playerHealth = playerHealth + 100 >= GameSessionData.playerMaxHealth ? GameSessionData.playerMaxHealth : playerHealth + 100;
        GameSessionData.playerHealth = playerHealth;
        refresh();
        validateRepairButtons();
    }

    void onFullFixCLicked()
    {
        playerCash -= GameSessionData.fullFixPrice;
        playerHealth = GameSessionData.playerMaxHealth;
        GameSessionData.playerHealth = playerHealth;
        refresh();
        validateRepairButtons();
    }

    void onHealthUpgradesClicked()
    {
        firstMainButtonRow.SetActive(false);
        secondMainButtonRow.SetActive(false);
        healthUpgradeButtonRow.SetActive(true);
        btnBack.SetActive(true);
        setHealthButtonsText();
        validateHealthButtons();
        btnSmallHealthUpgrade.onClick.AddListener(onSmallHealthUpgradeClicked);
        btnBigHealthUpgrade.onClick.AddListener(onBigHealthUpgradeClicked);
    }

    void onSmallHealthUpgradeClicked()
    {
        playerCash -= GameSessionData.smallHealthUpgradePrice;
        playerMaxHealth += 100;
        GameSessionData.playerMaxHealth = playerMaxHealth;
        increaseSmallHealthUpgradePrice(50);
        refresh();
        validateHealthButtons();
    }

     void onBigHealthUpgradeClicked()
    {
        playerCash -= GameSessionData.bigHealthUpgradePrice;
        playerMaxHealth += 300;
        GameSessionData.playerMaxHealth = playerMaxHealth;
        increaseBigHealthUpgradePrice(100);
        refresh();
        validateHealthButtons();
    }

    void onDamageClicked()
    {
        firstMainButtonRow.SetActive(false);
        secondMainButtonRow.SetActive(false);
        damageUpgradeButtonRow.SetActive(true);
        btnBack.SetActive(true);
        setDamageButtonsText();
        validateDamageButtons();
        btnSmallDamageUpgrade.onClick.AddListener(onSmallDamageClicked);
        btnBigDamageUpgrade.onClick.AddListener(onBigDamageClicked);
    }

    private void onSmallDamageClicked()
    {
        playerCash -= GameSessionData.smallDamagePrice;
        playerDamage += 1;
        GameSessionData.playerActualDamage = playerDamage;
        increaseSmallDamageUpgradePrice(50);
        refresh();
        validateDamageButtons();
    }

    private void onBigDamageClicked()
    {
        playerCash -= GameSessionData.bigDamagePrice;
        playerDamage += 3;
        GameSessionData.playerActualDamage += playerDamage;
        increaseBigDamageUpgradePrice(100);
        refresh();
        validateDamageButtons();
    }

    void setHealthButtonsText()
    {
        btnSmallHealthUpgrade.GetComponentInChildren<Text>().text = "";
        btnSmallHealthUpgrade.GetComponentInChildren<Text>().text = "Small health upgrade \n(" + GameSessionData.smallHealthUpgradePrice + ")";
        btnBigHealthUpgrade.GetComponentInChildren<Text>().text = "";
        btnBigHealthUpgrade.GetComponentInChildren<Text>().text = "Big health upgrade \n(" + GameSessionData.bigHealthUpgradePrice + ")";
    }

    void setDamageButtonsText()
    {
        btnSmallDamageUpgrade.GetComponentInChildren<Text>().text = "";
        btnSmallDamageUpgrade.GetComponentInChildren<Text>().text = "Small damage upgrade \n(" + GameSessionData.smallDamagePrice + ")";
        btnBigDamageUpgrade.GetComponentInChildren<Text>().text = "";
        btnBigDamageUpgrade.GetComponentInChildren<Text>().text = "Big damage upgrade \n(" + GameSessionData.bigDamagePrice + ")";
    }

    void increaseSmallHealthUpgradePrice(int amount)
    {
        GameSessionData.smallHealthUpgradePrice += amount;

        if (GameSessionData.smallHealthUpgradePrice >= GameSessionData.bigHealthUpgradePrice)
        {
            GameSessionData.bigHealthUpgradePrice += amount * 3;
        }
        setHealthButtonsText();
    }

    void increaseBigHealthUpgradePrice(int amount)
    {
        GameSessionData.bigHealthUpgradePrice += amount;
        GameSessionData.smallHealthUpgradePrice += amount / 4;
        setHealthButtonsText();
    }

    void increaseSmallDamageUpgradePrice(int amount)
    {
        GameSessionData.smallDamagePrice += amount;

        if (GameSessionData.smallDamagePrice >= GameSessionData.bigDamagePrice)
        {
            GameSessionData.bigDamagePrice += amount * 3;
        }
        setDamageButtonsText();
    }

    void increaseBigDamageUpgradePrice(int amount)
    {
        GameSessionData.bigDamagePrice += amount;
        GameSessionData.smallDamagePrice += amount / 4;
        setDamageButtonsText();
    }

    void validateRepairButtons()
    {
        btnFullFix.enabled = playerCash >= GameSessionData.fullFixPrice && playerHealth != playerMaxHealth ? true : false;
        btnBasicFix.enabled = playerCash >= GameSessionData.basicFixPrice && playerHealth != playerMaxHealth ? true : false;
    }

    void validateHealthButtons()
    {
        btnBigHealthUpgrade.enabled = (playerCash >= GameSessionData.bigHealthUpgradePrice) ? true : false;
        btnSmallHealthUpgrade.enabled = (playerCash >= GameSessionData.smallHealthUpgradePrice) ? true : false;       
    }

    void validateDamageButtons()
    {
        btnBigDamageUpgrade.enabled = (playerCash >= GameSessionData.bigDamagePrice) ? true : false;
        btnSmallDamageUpgrade.enabled = (playerCash >= GameSessionData.smallDamagePrice) ? true : false;
    }

    void refresh()
    {
        GameSessionData.cash = playerCash;
        playerCashText.GetComponent<Text>().text = "";
        playerHealthText.GetComponent<Text>().text = "";
        playerDamageText.GetComponent<Text>().text = "";

        playerCashText.GetComponent<Text>().text = playerCash + "";
        playerHealthText.GetComponent<Text>().text = playerHealth + "/" + playerMaxHealth;
        playerDamageText.GetComponent<Text>().text = playerDamage + "";
    }
}*/
