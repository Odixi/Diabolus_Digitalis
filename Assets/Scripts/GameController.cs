﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;
using System;

public class GameController : MonoBehaviour
{
    public bool test;
	public int startWave = 1;

	public enum GameState
	{
		START,
		PLAY,
		GAME_OVER,
		ROUND_OVER,
		DIALOG_PAUSE
	}

	public int enemyTypeCountMax, enemyTypeCountMin;

	public GameObject playAudio;
    private GameObject playAudioInstance;
	public GameObject endAudio;
	private GameObject startText;
	private GameObject introText;
	private GameObject roundOverText;
	private GameObject gameOverCanvas;
	private GameObject pauseCanvas;
	private GameObject DialogCanvas;
	private GameObject hudCanvas;

	public GameState state;
	public float enemyResourceDropRateMultipler;

	private int roundNumber;
	public float roundTime;
	public float startWait;
	public float waveWait;

	private PlayerController player;
	private DatabaseController dbController;

	private bool paused = false;
	private bool gameOver = false;
	private bool spawnOnce = false;
	private bool canSaveHighScore = true;
	private bool roundOver = false;

	public static AudioSource fxSoundSource;
	// Käyttäkää tässä ainoastaan PlayOneShot(AudioClip) metodia
	public float fxSoundVolume;
	public float musicVolume;
	public GameObject gameDataController;

    public float fadeTime = 1;

    private DialogManager dialogManager;
    private GameObject dialogBox;
    private bool hasActiveDialogs = false;
    private bool nextRoundDialogs = false;

    private bool bossPaused = false;

    void Awake()
	{	
		GameObject gsd = GameObject.FindGameObjectWithTag("GameSessionData");
		if (gsd != null)
		{
			if (GameSessionData.gameOver == true)
			{
				gsd.GetComponent<GameSessionData>().Restart();
			}
		}
		else
		{
			Instantiate(gameDataController);
			GameSessionData.wave = startWave;
			roundNumber = GameSessionData.wave;
		}

		#if UNITY_EDITOR
		switch(SceneManager.GetActiveScene().name){
		case ("ArcadeMode"):
			GameSessionData.mode = GameMode.ARCADE;
			break;
		case ("StoryMode"):
			GameSessionData.mode = GameMode.STORY;
			break;
		case ("TestingScene"):
			GameSessionData.mode = GameMode.TEST;
			break;
		}
		#endif

		if (!GameSessionData.isArcadeMode()) {
			GameSessionData.upgradeSuperLaser = 1;
		}
	}

	void Start()
	{
        if (test)
        {
            GameSessionData.mode = GameMode.TEST;
        }
        
		Cursor.visible = false;

		GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
		GameObject dbObject = GameObject.Find("DatabaseController");

		player = playerObject.GetComponent<PlayerController>();
		dbController = dbObject.GetComponent<DatabaseController>();

		startText = GameObject.FindGameObjectWithTag("StartText");
		roundOverText = GameObject.FindGameObjectWithTag("OverText");
		gameOverCanvas = GameObject.FindGameObjectWithTag("EndCanvas");
		pauseCanvas = GameObject.Find("PauseCanvas");
		introText = GameObject.FindGameObjectWithTag("IntroText");
		if(GameSessionData.isStoryMode()){
			DialogCanvas = GameObject.FindGameObjectWithTag ("DialogCanvas");
			DialogCanvas.GetComponent<Canvas> ().enabled = true;
		}
		hudCanvas = GameObject.FindGameObjectWithTag ("HUDCanvas");

		roundOverText.GetComponent<Text>().enabled = false;
		startText.GetComponent<Text>().enabled = true;
		gameOverCanvas.GetComponent<Canvas>().enabled = false;

		pauseCanvas.GetComponent<Canvas>().enabled = false;
		roundNumber = GameSessionData.wave;

		if (roundNumber != 1)
		{
			introText.GetComponent<Text>().enabled = false;
		}

		if (roundNumber == 1)
		{
			introText.GetComponent<Text>().enabled = true;
		}

        if (GameSessionData.mode == GameMode.STORY)
        {
            introText.SetActive(false);
            startText.SetActive(false);
        }

		fxSoundSource = gameObject.AddComponent<AudioSource>();
		fxSoundSource.volume = fxSoundVolume;

		playAudioInstance = Instantiate(playAudio);
		playAudioInstance.GetComponent<AudioSource>().volume = musicVolume;


        if (GameSessionData.isStoryMode())
        {
            dialogManager = new DialogManager(PlayerPrefs.GetString("PlayerName"));
            dialogBox = GameObject.Find("DialogBox");
            canSaveHighScore = false;
            GameObject.Find("Score_text").SetActive(false);
            GameObject.Find("Resource_text").SetActive(false);
            GameObject.Find("Wave_text").SetActive(false);
        }

        state = GameState.START;

	}

	void Update()
	{
		
		if (state == GameState.ROUND_OVER && state != GameState.GAME_OVER)
		{
			// Tässä kohtaa siirrytään kauppa sceneen ja sitä ennen tallennetaan tiedot gamesessiondataan
			if (!roundOver)
			{
				roundOver = true;
				roundNumber = roundNumber + 1;
				GameSessionData.cash = PlayerController.resource;
				GameSessionData.playerHealth = player.health;
				GameSessionData.playerMaxHealth = player.maxHealth;
				GameSessionData.score = PlayerController.score;
				GameSessionData.wave = roundNumber;
				if (!player.destroyed && GameSessionData.isArcadeMode())
                {
                    SceneManager.LoadScene("Store");
                }
				if (!player.destroyed && GameSessionData.isStoryMode()) {  

					//player.resetHealth ();
					state = GameState.DIALOG_PAUSE;

				}
            }
		}

		if(state == GameState.DIALOG_PAUSE && !player.destroyed){

			DialogCanvas.GetComponent<Canvas> ().enabled = true;
			hudCanvas.GetComponent<Canvas>().enabled = false;
			player.setCutscene (true);
			spawnOnce = false;
            roundOver = false;

            int lastRound = roundNumber - 1;          

            switch (lastRound)
            {
                case 1:
                    if (dialogManager.getFirstDialogsCount() == 3)
                    {
                        StartCoroutine(paceDialogs(3));
                    }else if(!hasActiveDialogs)
                    {
                        nextRoundDialogs = true;
                    }                  

                    break;

                case 2:
                    if (dialogManager.getSecondDialogsCount() == 7)
                    {
                        StartCoroutine(paceDialogs(7));
                    } else if (!hasActiveDialogs)
                    {
                        nextRoundDialogs = true;
                    }
                    break;

                case 3:
                    if (dialogManager.getThirdDialogsCount() == 2)
                    {
                        StartCoroutine(paceDialogs(2));
                    }
                    else if (!hasActiveDialogs)
                    {
                        nextRoundDialogs = true;
                    }
                    break;

                case 4:
                    if (dialogManager.getFourthDialogsCount() == 2)
                    {
                        StartCoroutine(paceDialogs(2));
                    }
                    else if (!hasActiveDialogs)
                    {
                        nextRoundDialogs = true;
                    }
                    break;
                
                case 5:
                    if (dialogManager.getBossDialogsCount() == 1)
                    {
                        StartCoroutine(paceDialogs(1));
                    }
                    else if (!hasActiveDialogs)
                    {
                        nextRoundDialogs = true;
                    }
                    break;


                default: break;
            }

            if (nextRoundDialogs)
            {

                switch (GameSessionData.wave)
                {
                    case 2:

                        if (dialogManager.getSecondDialogsCount() == 9)
                        {
                            StartCoroutine(paceDialogs(2));
                        }else if (!hasActiveDialogs)
                        {
                            nextRoundDialogs = false;
                            state = GameState.PLAY;
                        }

                        break;

                    case 3:
                        if (dialogManager.getThirdDialogsCount() == 3)
                        {
                            StartCoroutine(paceDialogs(1));
                        }
                        else if (!hasActiveDialogs)
                        {
                            nextRoundDialogs = false;
                            state = GameState.PLAY;
                        }
                        break;

                    case 4:
                        if (dialogManager.getFourthDialogsCount() == 4)
                        {
                            StartCoroutine(paceDialogs(2));
                        }
                        else if (!hasActiveDialogs)
                        {
                            nextRoundDialogs = false;
                            state = GameState.PLAY;
                        }
                        break;

                    case 5:
                        if (dialogManager.getBossDialogsCount() == 7)
                        {
                            StartCoroutine(paceDialogs(6));
                        }
                        else if (!hasActiveDialogs)
                        {
                            nextRoundDialogs = false;
                            state = GameState.PLAY;
                        }
                        break;

				case 6:
					SceneManager.LoadScene ("EndingCutscene");
                        break;


                    default: break;
                }


                StartCoroutine(Dialogi());
            }else
            {
                state = GameState.DIALOG_PAUSE;
            }
			print (roundNumber);

		}

		if (state == GameState.START)
		{
            if (GameSessionData.mode == GameMode.STORY)
            {

                if (dialogManager.getFirstDialogsCount() == 5)
                {
                    StartCoroutine(paceDialogs(2));
                }

                else if (!hasActiveDialogs)
                {
                    introText.SetActive(true);
                    startText.SetActive(true);
                    introText.GetComponent<Text>().enabled = true;
                    startText.GetComponent<Text>().enabled = true;

                    if (Input.anyKeyDown)
                    {
                        introText.GetComponent<Text>().enabled = false;
                        startText.GetComponent<Text>().enabled = false;
                        state = GameState.PLAY;
                    }
                }
            }
            else
            {
                PlayerController.resource = GameSessionData.cash;

                if (Input.anyKeyDown)
                {
                    startText.GetComponent<Text>().enabled = false;
                    if (roundNumber == 1)
                    {
                        introText.GetComponent<Text>().enabled = false;
                    }

                    state = GameState.PLAY;
                }
            }
		}

		if (state == GameState.PLAY)
		{
			if (!spawnOnce)
			{			
				StartCoroutine(SpawnWavesNew());
			}

			if (Input.GetKeyDown(KeyCode.P) || Input.GetButtonDown("Cancel"))
			{
				pausePressed();
			}
		}
	}
    IEnumerator paceDialogs(int dialogAmount)
    {
        hasActiveDialogs = true;
        DialogCanvas.SetActive(true);

        for (int i = 0; i < dialogAmount; i++)
        {
            Dialog dialog = dialogManager.nextDialog();

            GameObject.Find("SpeakerText").GetComponent<Text>().text = dialog.getSpeaker();

            GameObject.FindGameObjectWithTag("ingameDialogText")
                .GetComponent<Text>().text =  dialog.getText();
                
            yield return new WaitForSeconds(dialog.getFadeOutTime());
        }

        hasActiveDialogs = false;
        DialogCanvas.SetActive(false);

        yield break;
    }



    /*
     * The actual enemy spawing is handled in EnemySpawner.
     */
    IEnumerator SpawnWavesNew()
	{

        print(GameSessionData.mode);

		spawnOnce = true;
		yield return new WaitForSeconds(startWait);

		// Determine whether an arcade mode round or a story mode round should be spawned, and call the appropriate function 
		if (GameSessionData.isArcadeMode()) {
			yield return StartCoroutine (GetComponent<EnemySpawner> ().SpawnArcadeRound (roundNumber));
		} else if (GameSessionData.isStoryMode()) {
			yield return StartCoroutine (GetComponent<EnemySpawner> ().SpawnStoryModeRound (roundNumber));
		} else {
			yield return StartCoroutine (GetComponent<EnemySpawner> ().SpawnTestRound (roundNumber));
		}
		yield return new WaitForSeconds(5);

		if (!player.destroyed && GameSessionData.isArcadeMode())
		{
			roundOverText.GetComponent<Text>().enabled = true;
		}
		
		yield return new WaitForSeconds(waveWait);
		if (state != GameState.GAME_OVER)
		{
			state = GameState.ROUND_OVER;
		}
	}

	public void GameOver()
	{
        GameSessionData.gameOver = true;
        state = GameState.GAME_OVER;
		StopAllCoroutines ();

        Cursor.visible = true;

        GameObject btnMainMenu = GameObject.Find("EndButtonMainMenu");
        GameObject btnRestart = GameObject.Find("EndButtonRestart");
        GameObject btnQuit = GameObject.Find("EndButtonQuit");

        if (GameSessionData.isStoryMode())
        {
            GameObject.Find("EndScoreText").SetActive(false);
            btnMainMenu.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
            btnRestart.GetComponent<RectTransform>().localPosition = new Vector3(-160, 0, 0);
            btnQuit.GetComponent<RectTransform>().localPosition = new Vector3(160, 0, 0);
        }
        else
        {
            GameObject.Find("EndScoreText").GetComponent<Text>().text = 
                "Score " + PlayerController.score.ToString();
        }	

		btnMainMenu.GetComponent<Button>().onClick.AddListener(onMainMenuClicked);
		btnRestart.GetComponent<Button>().onClick.AddListener(onRestartClicked);
		btnQuit.GetComponent<Button>().onClick.AddListener(onQuitClicked);

		gameOverCanvas.GetComponent<Canvas>().enabled = true;

		gameOverCanvas.SetActive(true);

		if (GameObject.FindGameObjectWithTag("PlayAudio"))
		{
			Destroy(GameObject.FindGameObjectWithTag("PlayAudio"));
			Instantiate(endAudio);
		}

		if (canSaveHighScore)
		{
            HighScore highScore = new HighScore(PlayerPrefs.GetString("PlayerName"), PlayerController.score);

            if (dbController.canSave(highScore))
			{
				dbController.save(highScore);
			}

			canSaveHighScore = false;

            List<HighScore> hs = dbController.getHighScores();

            var hsText = "High scores\n";
            for (int i = 0; i < Mathf.Min(hs.Count, 5); i++)
            {
                hsText = hsText + "#" + (i + 1).ToString() + " " + hs[i].name + " - " + hs[i].score + "\n";
            }

            GameObject.Find("HighscoreText").GetComponent<Text>().text = hsText;
        }
        else
        {
            GameObject.Find("GameOverText").GetComponent<Text>().alignment = TextAnchor.LowerCenter;
            GameObject.Find("HighscoreText").SetActive(false);
        }
		
	}

	void pausePressed()
	{
		if (paused)
		{
			//player.resetHealth ();
			StartCoroutine(fadeInSound());

			var btnResume = GameObject.Find("PauseButtonResume").GetComponent<Button>();
			var btnMainMenu = GameObject.Find("PauseButtonMainMenu").GetComponent<Button>();
			var btnQuit = GameObject.Find("PauseButtonQuit").GetComponent<Button>();

			btnResume.onClick.RemoveAllListeners();
			btnMainMenu.onClick.RemoveAllListeners();
			btnQuit.onClick.RemoveAllListeners();

			Cursor.visible = false;
			pauseCanvas.GetComponent<Canvas>().enabled = false;
			Time.timeScale = 1;
			paused = false;

		}
		else
		{
			StartCoroutine(fadeOutSound());
			Cursor.visible = true;
			pauseCanvas.GetComponent<Canvas>().enabled = true;

			var btnResume = GameObject.Find("PauseButtonResume").GetComponent<Button>();
			var btnMainMenu = GameObject.Find("PauseButtonMainMenu").GetComponent<Button>();
			var btnQuit = GameObject.Find("PauseButtonQuit").GetComponent<Button>();

			btnResume.onClick.AddListener(onResumeClicked);
			btnMainMenu.onClick.AddListener(onMainMenuClicked);
			btnQuit.onClick.AddListener(onQuitClicked);         

			Time.timeScale = 0;
			paused = true;
		}
	}

	IEnumerator fadeOutSound()
	{
		float t = fadeTime;
		while (t > 0)
		{
			yield return null;
			t -= 0.01f;
			AudioListener.volume = t / fadeTime;
		}
		yield break;
	}

	IEnumerator fadeInSound()
	{
		float t = 0;
		while (t <= fadeTime)
		{
			yield return null;
			t += 0.01f;
			AudioListener.volume = t / fadeTime;
		}
		yield break;
	}

	void onMainMenuClicked()
	{
		AudioListener.volume = 1.0f;
		GameSessionData.gameOver = true;
		Time.timeScale = 1;
		SceneManager.LoadScene("Main_Menu");
	}

	void onRestartClicked()
	{
		AudioListener.volume = 1.0f;
		GameSessionData.gameOver = true;
		SceneManager.LoadScene(GameSessionData.getGameModeToLoad());
	}

	void onQuitClicked()
	{
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
		    Application.Quit();
        #endif       
    }

	void onResumeClicked()
	{
		pausePressed();
	}

	IEnumerator Dialogi(){
        yield return new WaitForSeconds(1);
		yield return new WaitWhile(() => hasActiveDialogs);
		DialogCanvas.GetComponent<Canvas> ().enabled = false;
		hudCanvas.GetComponent<Canvas>().enabled = true;
		player.setCutscene (false);

        state = GameState.PLAY;	
	}

    public void startPaceBossDialogs(GameObject boss)
    {
        StartCoroutine(paceBossDialogs(boss));
    }

    IEnumerator paceBossDialogs(GameObject boss)
    {
        bool hasBossDialogs = true;

        while (hasBossDialogs)
        {
            if (dialogManager.getBossDialogsCount() == 0)
            {
                hasBossDialogs = false;
                break;
            }
            else if (boss == null && dialogManager.getBossPauseDialogsCount() == 1)
            {
                bossPaused = true;
            }
            else if (boss.GetComponent<EntityUserData>().health < 3500 &&
                 dialogManager.getBossPauseDialogsCount() == 3)
            {
                bossPaused = true;
            }
            else if (boss.GetComponent<EntityUserData>().health <= 1500 &&
                dialogManager.getBossPauseDialogsCount() == 2)
            {
                bossPaused = true;
            }

            if (bossPaused)
            {
                hasActiveDialogs = true;
                DialogCanvas.SetActive(true);
                DialogCanvas.GetComponent<Canvas>().enabled = true;

                Dialog dialog = dialogManager.nextBossPauseDialog();

                GameObject.Find("SpeakerText").GetComponent<Text>().text = dialog.getSpeaker();

                GameObject.FindGameObjectWithTag("ingameDialogText")
                    .GetComponent<Text>().text = dialog.getText();

                yield return new WaitForSeconds(dialog.getFadeOutTime());  

                hasActiveDialogs = false;
                DialogCanvas.GetComponent<Canvas>().enabled = false;
                DialogCanvas.SetActive(false);
                bossPaused = false;
            }

            yield return null;
        }
        yield break;
    }
}