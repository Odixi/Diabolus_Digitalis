﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaAnim : MonoBehaviour {

	public Vector2 speedMain= new Vector2(0.0003f,0.0002f);

	private Material material;

	void Start(){
		material = GetComponent<Renderer> ().material;
	}

	void LateUpdate () {
		material.mainTextureOffset = material.mainTextureOffset + speedMain;
	}
}
