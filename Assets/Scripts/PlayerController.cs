﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class PlayerController : MonoBehaviour
{
    public float fireRate;
    public float speed;
    public float tilt;
    public Boundary boundary;
    public GameObject shot;
    public List<Transform> frontShotSpawns;
    public List<Transform> sideShotSpawns;

    private float nextFire;
    private Rigidbody rb;

	public int health;
	public int shield;
	public Slider healthSlider;
	public Slider shieldSlider;

	public Image SuperLaserIcon;
	public Text SuperLaserCooldownText;

	public GameObject superLaser;
	public float superLaserUpgradeDmgMultipler = 1.15f;
	public float superLaserMaxCooldown = 90;
	public float superLaserMinCoolDown = 45;
	private float superLaserCooldown;
	private float timeFromLastSLaser;

	public GameObject shieldSphere;
	public AnimationCurve shieldTransparencyCurve;
	public float shieldVisibilityTime;
	private Color shieldBaseColor;

	public static int score; // Varmaan fiksumpiakin tapoja löytyy kun staattisen käyttö mutta oh well
	public static int resource;
    public Text healthText;
    public Text scoreText;
	public Text resourceText;
	public Text shieldText;

	public AudioSource laser1;

    public bool ready { get; set; }
    public bool destroyed { get; set; }
	private bool cutscene = false;

    private Image healthColor;
    public int maxHealth;
	public int maxShield;

	private float dodgeAngle;
	private short dodgeRight; // +1 = right, -1 = left

	public float dodgeSpeed;
	public float dodgeDuration;

	private float nextDodge;
	public float dodgeRate;
	public float dodgeDeacc;

	public float shieldWaitTime;
	public float shieldRechargeRate;
	private float timeFromLastHit;
	private float timeFromLastHitToShield;
	private float shieldRechargeAssistant;

    void Start()
    {
		score = GameSessionData.score;
		resource = GameSessionData.cash;
        ready = false;
        destroyed = false;
        rb = GetComponent<Rigidbody>();
		health = GameSessionData.playerHealth;
		maxHealth = GameSessionData.playerMaxHealth;
		shield = GameSessionData.playerMaxShield;
		GameSessionData.playerShield = GameSessionData.playerMaxShield;
		this.GetComponent<PlayerUserData> ().shield = shield;
		maxShield = GameSessionData.playerMaxShield;

		this.GetComponent<EntityUserData> ().health = GameSessionData.playerHealth;
        shot.GetComponent<EntityUserData>().damage = GameSessionData.playerActualDamage;
        healthColor = healthSlider.GetComponentsInChildren<Image>().FirstOrDefault(t => t.name == "Fill");
		healthSlider.maxValue = GameSessionData.playerMaxHealth;
		healthSlider.GetComponentInChildren<Text> ().text = health.ToString();
		shieldSlider.maxValue = GameSessionData.playerMaxShield;
		shieldSlider.GetComponentInChildren<Text> ().text = shield.ToString ();
		dodgeAngle = -1;
		timeFromLastSLaser = superLaserMaxCooldown; // So that player can shoot super laser right away

		timeFromLastHitToShield = 10;
		shieldBaseColor = shieldSphere.GetComponent<MeshRenderer> ().material.color;
		shieldSphere.GetComponent<MeshRenderer> ().material.color = new Color (0, 0, 0, 0);

		if (GameSessionData.upgradeSuperLaser == 0 && GameSessionData.isArcadeMode()) {
			SuperLaserCooldownText.enabled = false;
			SuperLaserIcon.enabled = false;
			SuperLaserIcon.transform.GetComponentInChildren<Text> ().enabled = false;
		} else {
			SuperLaserCooldownText.enabled = true;
			SuperLaserIcon.enabled = true;
			SuperLaserIcon.transform.GetComponentInChildren<Text> ().enabled = true;
		}
		float slcde = superLaserMaxCooldown - superLaserMinCoolDown;
		if (GameSessionData.upgradeSuperLaser != 0) {
			superLaserCooldown = superLaserMaxCooldown + (slcde / (GameSessionData.upgradeSuperLaser * 0.8f)) - slcde*(1f/0.8f);
			print (superLaserCooldown);
		} else {
			superLaserCooldown = superLaserMaxCooldown;
		}
    }

    void Update()
    {

		health = this.GetComponent<EntityUserData> ().health;
		healthSlider.value = health;
		healthSlider.GetComponentInChildren<Text> ().text = health.ToString();

		shield = this.GetComponent<PlayerUserData> ().shield;
		shieldSlider.value = shield;
		shieldSlider.GetComponentInChildren<Text> ().text = shield.ToString ();

        if (health <= maxHealth / 4)
        {
            healthText.color = Color.red;
            healthColor.GetComponent<Image>().color = Color.red;
            
        }
        else if (health <= maxHealth / 2)
        {
            healthText.color = Color.yellow;
            healthColor.GetComponent<Image>().color = Color.yellow;
        }
        else if (health > maxHealth)
        {
            healthText.color = Color.magenta;
            healthColor.GetComponent<Image>().color = Color.magenta;
        }
        else if (health <= maxHealth && health > maxHealth / 2)
        {
            healthText.color = Color.green;
            healthColor.GetComponent<Image>().color = Color.green;
        }

		scoreText.text = score.ToString();
		resourceText.text = resource.ToString ();
		SuperLaserCooldownText.text = timeFromLastSLaser > superLaserCooldown ? "" : Mathf.Ceil (superLaserCooldown - timeFromLastSLaser).ToString();
		SuperLaserIcon.color = timeFromLastSLaser > superLaserCooldown ? Color.white : new Color(0.5f,0.5f,0.5f,0.5f);
    }

    void FixedUpdate()
    {

		if (Input.GetButton("Fire1") && Time.time > nextFire && !cutscene)
		{
			nextFire = Time.time + fireRate;
            foreach (Transform shotSpawn in frontShotSpawns) {
                Instantiate(shot, shotSpawn.position, new Quaternion());
			    laser1.Play ();
            }
		}
		if (Input.GetButton("Fire3") && Time.time > nextFire && !cutscene)
        {
            nextFire = Time.time + fireRate;
            foreach (Transform shotSpawn in sideShotSpawns) {
                Instantiate(shot, shotSpawn.position, Quaternion.Euler(0, shotSpawn.rotation.eulerAngles.y, 0));
                laser1.Play ();
            }
        }

		if (Input.GetButton("Dodge") && dodgeAngle == -1 && Time.time > nextDodge){
			nextDodge = Time.time + dodgeRate;
			float lr = Input.GetAxis ("Horizontal");
			if (lr > 0) {
				dodgeRight = 1;
				dodgeAngle = 0;
			} else if (lr < 0) {
				dodgeRight = -1;
				dodgeAngle = 0;
			}
		}
		if (Input.GetButton ("SuperLaser") && timeFromLastSLaser > superLaserCooldown && (GameSessionData.upgradeSuperLaser > 0 || !GameSessionData.isArcadeMode())) {
			GameObject sl = Instantiate (superLaser, transform.position + new Vector3(0,0.3f,12.5f), transform.rotation);
			SuperLaserCollisionManager slcm = sl.GetComponent<SuperLaserCollisionManager> ();
			slcm.damagePerTic = (int)(Mathf.Pow (superLaserUpgradeDmgMultipler, GameSessionData.upgradeSuperLaser) * slcm.damagePerTic);
			slcm.damagePerTicForBosses = (int)(Mathf.Pow (superLaserUpgradeDmgMultipler, GameSessionData.upgradeSuperLaser) * slcm.damagePerTicForBosses);
			sl.transform.parent = this.transform;
			timeFromLastSLaser = 0;
		}

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.velocity = movement * speed;

		// if dodging, then...
		if (dodgeAngle != -1) {
			Vector3 dodgeMov = new Vector3 (dodgeSpeed * dodgeRight ,0,0);
			rb.velocity = movement * speed + dodgeMov;
			rb.rotation = Quaternion.Euler (0, 0, dodgeAngle * -dodgeRight + rb.velocity.x * -tilt); 
			//dodgeAngle += Mathf.Min(dodgeDuration + dodgeDeacc / Mathf.Max(50f, dodgeAngle), 360);
			dodgeAngle += dodgeDuration;
			// when a whole dodge is complete
			if (dodgeAngle >= 360) {
				dodgeAngle = -1;
			}
		}

        rb.position = new Vector3
        (
            Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp(rb.position.z, boundary.zMin, boundary.zMax)
        );

		if (dodgeAngle == -1) {
			rb.rotation = Quaternion.Euler (0.0f, 0.0f, rb.velocity.x * -tilt);
		}

		// Shield takes damage
		if (timeFromLastHitToShield < shieldVisibilityTime) {
			shieldBaseColor.a = shieldTransparencyCurve.Evaluate (timeFromLastHitToShield / shieldVisibilityTime);
			shieldSphere.GetComponent<MeshRenderer> ().material.color = shieldBaseColor;
		}

		if (timeFromLastHit > shieldWaitTime) {
			if (shield < maxShield) {
				shieldRechargeAssistant += Time.fixedDeltaTime / shieldRechargeRate;
				if (shieldRechargeAssistant >= 1) {
					this.GetComponent<PlayerUserData> ().shield += 1;
					shield += 1;
					shieldRechargeAssistant -= 1;
				}
			}
		}
		timeFromLastHit += Time.fixedDeltaTime;
		timeFromLastHitToShield += Time.fixedDeltaTime;
		timeFromLastSLaser += Time.fixedDeltaTime;
    }//FixedUpdateEND
	public void resetHealth(){
		this.GetComponent<EntityUserData> ().health = GameSessionData.playerMaxHealth;
        healthText.color = Color.green;
        healthColor.GetComponent<Image>().color = Color.green;
    }

    private void OnDestroy()
    {
        if (!healthSlider.IsDestroyed())
        {
            health = this.GetComponent<EntityUserData>().health;
            scoreText.text = score.ToString();
            healthSlider.value = health;
            healthSlider.GetComponentInChildren<Text>().text = health.ToString();
        }

        destroyed = true;
    }

	public void setCutscene(bool a){
		this.cutscene = a;
	}

	// When player is hit shield recharging stops
	public void whenHit(){
		shield = this.GetComponent<PlayerUserData> ().shield;
		timeFromLastHit = 0;
		timeFromLastHitToShield = shield > 0 ? 0 : timeFromLastHitToShield;
	}

	public void Die(){
		foreach (Transform childTrans in transform) {
			Destroy (childTrans.gameObject);
		}
		foreach (Component c in GetComponents<Component>()) {
			if (c != transform) {
				Destroy (c);
			}
		}
	}
}

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}