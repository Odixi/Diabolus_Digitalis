﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByBoundary : MonoBehaviour
{

    void OnTriggerExit(Collider other)
    {
        // If other is an enemy ship, destroy its movement curve if it has one
		if (other.tag == "Enemy" || other.tag == "Boss")
        {
            Destroy(other.gameObject.GetComponent<CurveMover>().movementCurve.gameObject);
        }
		if (other.tag != "BossTransformer") {
			Destroy (other.gameObject);
		}
    }
}
