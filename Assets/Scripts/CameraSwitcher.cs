﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitcher : MonoBehaviour {
    private bool ab = true;
    private GameObject player;
    public Camera Cam1;
    public Camera MainCamera;
    
    // Use this for initialization
    private void Awake()
    {
        MainCamera = Camera.main;
        player = GameObject.FindGameObjectWithTag("Pelaaja");
        Cam1 = GameObject.FindGameObjectWithTag("cam1").GetComponent<Camera>();
        Cam1.enabled = false;
        MainCamera.enabled = true;
    }

    void Start() {
        
}



    // Update is called once per frame
    void Update () {
        transform.LookAt(player.transform);

        if (Input.GetKeyDown(KeyCode.C) && ab)
        {
            Cam1.enabled = true;
            MainCamera.enabled = false;
            ab = false;
        }
        else if(Input.GetKeyDown(KeyCode.C) && !ab)
        {
            Cam1.enabled = false;
            MainCamera.enabled = true;
            ab = true;
        }
    }
}

