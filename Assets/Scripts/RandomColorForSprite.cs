﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColorForSprite : MonoBehaviour {

	public Gradient gradient;
	public float speed;

	private SpriteRenderer spriteRend;
	private float currentColor = 0;

	// Use this for initialization
	void Start () {
		spriteRend = GetComponent<SpriteRenderer> ();
	}

	// Update is called once per frame
	void Update () {
		spriteRend.color = gradient.Evaluate (currentColor);
		currentColor += Time.deltaTime * speed;
		currentColor = currentColor > 1 ? currentColor - 1 : currentColor;
	}
}
