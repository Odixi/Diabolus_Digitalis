﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColorForMaterial : MonoBehaviour {

	public Gradient gradient;
	public float speed;

	private Renderer rend;
	private float currentColor = 0;

	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer> ();
	}

	// Update is called once per frame
	void Update () {
		rend.material.SetColor("_TintColor", gradient.Evaluate (currentColor));
		currentColor += Time.deltaTime * speed;
		currentColor = currentColor > 1 ? currentColor - 1 : currentColor;
	}
}
